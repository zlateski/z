<?php

class session
{
    static function start()
    {
        session_start();
    }

    static function get($what)
    {
        return isset($_SESSION[$what]) ? $_SESSION[$what] : null;
    }

    static function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }
}