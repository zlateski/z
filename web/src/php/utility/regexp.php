<?php

class regexp
{
    const EMAIL  = "([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*[0-9a-zA-Z]\.)+[a-zA-Z]{2,9})";
    const SCALAR = "[0-9]{1,16}";
    const HEX    = "[0-9a-fA-F]{1,64}";
    const URL    = "(((ht|f)tp(s?):\/\/)|(www\.[^ \[\]\(\)\n\r\t]+)|(([012]?[0-9]{1,2}\.){3}[012]?[0-9]{1,2})\/)([^ \[\]\(\),;&quot;'&lt;&gt;\n\r\t]+)([^\. \[\]\(\),;&quot;'&lt;&gt;\n\r\t])|(([012]?[0-9]{1,2}\.){3}[012]?[0-9]{1,2})";

    const PRINTF = "%[\-\+0\s\#]{0,1}(\d+){0,1}(\.\d+){0,1}[hlI]{0,1}([cCdiouxXeEfgGnpsS]){1}";


    public static function is_scalar( $val )
    {
        return preg_match( "~^" . self::SCALAR . "$~", $val ) === 1;
    }

    public static function is_email( $val )
    {
        return preg_match( "~^" . self::EMAIL . "$~", $val ) === 1;
    }

    public static function is_hex( $val )
    {
        return preg_match( "~^" . self::HEX . "$~", $val ) === 1;
    }

    public static function is_url( $val )
    {
        return preg_match( "~^" . self::URL . "$~", $val ) === 1;
    }
}