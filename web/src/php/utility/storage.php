<?php namespace zi;

class storage
{
    private $prefix = null;

    private static $rnd_chars =
        array( 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
               'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
               's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0',
               '1', '2', '3', '4', '5', '6', '7', '8', '9' );

    private static function random_string($length = 20)
    {
        $res = '';
        for ( $i = 0; $i < $length; ++$i )
        {
            $res .= self::$rnd_chars[mt_rand(0, 35)];
        }
        return $res;
    }

    private static function random_dir_path($length = 3)
    {
        $res = '';
        for ( $i = 0; $i < $length; ++$i )
        {
            $res .= self::$rnd_chars[mt_rand(0, 35)] . '/';
        }
        return $res;
    }

    public static function random_file($dir_len = 3, $fname_len = 30)
    {
        return self::random_dir_path($dir_len) . self::random_string($fname_len);
    }

    public function __construct($prefix)
    {
        $this->prefix = $prefix . '/';
    }

    public static function at($prefix)
    {
        return new static($prefix);
    }

    public function get($path)
    {
        return file_get_contents($this->prefix . $path);
    }

    public function store_at($path, $data)
    {
        return file_put_contents($this->prefix . $path, $data);
    }

    public function store($data)
    {
        $fname = self::random_file();
        if ( $this->store_at($fname, $data) !== FALSE )
        {
            return $fname;
        }
        return FALSE;
    }

    private static function initialize_recursive($prefix, $depth)
    {
        mkdir($prefix);

        if ( $depth == 0 )
        {
            return;
        }

        for ( $i = 0; $i < 36; ++$i )
        {
            self::initialize_recursive($prefix . self::$rnd_chars[$i] . '/', $depth-1);
        }
    }

    // to be used only ti initialize dir structure at some dir
    public function initialize($depth = 3)
    {
        self::initialize_recursive($this->prefix, $depth);
    }
}

//$v = storage::at('./img')->store('pera') ;
//echo $v . "\n";
//echo storage::at('./img')->get($v) . "\n";