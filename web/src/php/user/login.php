<?php

require_once 'utility/sql.php';

class login
{
    public static function try_login($user_or_email, $pass_md5)
    {
        return sql::rowf(
            'SELECT * FROM `users` '.
            '    WHERE (`username` = "%s" AND `password` = "%s") '.
            '    OR (`email` = "%s" AND `password` = "%s")',
            $user_or_email, $pass_md5,
            $user_or_email, $pass_md5);
    }

    public static function try_from_cooke($cookie)
    {
        if (!$cookie)
        {
            return false;
        }

        $id = sql::rowf('SELECT `user_id` FROM `sessions` WHERE `session` = "%s"', $cookie);

        if ( $id !== false )
        {
            return sql::rowf('SELECT * FROM `users` '.
                             'WHERE `id` = %d', $id['user_id']);
        }
        
        return false;
    }


}


var_dump(login::try_from_cooke(''));