<?php

class user_type
{
    const REGULAR         = 0;
    const MODERATOR       = 1;
    const SUPER_MODERATOR = 2;
    const ADMIN           = 3;
}