DROP TABLE IF EXISTS `sessions`;
CREATE TABLE `sessions` (
  `user_id` int(11) NOT NULL,
  `session` varchar(32),
  `create_time` int(11) NOT NULL,
  `last_login` int(11) NOT NULL,
  PRIMARY KEY (`user_id`, `session`),
  INDEX (`session`),
  INDEX (`create_time`),
  INDEX (`last_login`)
) ENGINE=InnoDB AUTO_INCREMENT=1000001 DEFAULT CHARSET=utf8;
