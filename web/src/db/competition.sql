DROP TABLE IF EXISTS `competitions`;
CREATE TABLE `competitions` (
  `id` int(11) NOT NULL auto_increment,
  `type` smallint(6) NOT NULL,
  `name` varchar(100) NOT NULL,
  `info` text NOT NULL,
  `start_time` datetime NOT NULL,
  `end_time` datetime NOT NULL,
  `is_graded` smallint(6) NOT NULL default '0',
  `team_based` smallint(6) NOT NULL,
  `duration` int(11) NOT NULL,
  `pause_time` int(11) NOT NULL,
  `rated` smallint(6) NOT NULL,
  `approved` smallint(6) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8000001 DEFAULT CHARSET=utf8;
