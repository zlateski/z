DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL auto_increment,
  `username` varchar(32),
  `password` varchar(32),
  `email` varchar(128),
  `type` smallint(6) NOT NULL,
  `status` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX (`id`),
  INDEX (`username`),
  INDEX (`email`),
  INDEX (`type`),
  INDEX (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=1000001 DEFAULT CHARSET=utf8;
