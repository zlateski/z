/*
 * Copyright (c) 2009-2014 z-training -- http://z-trening.com
 *
 * AUTHORS:
 *   Aleksandar Zlateski (aleksandar.zlateski@gmail.com)
 *
 */

#include <stdio.h>

#include "zgrader/zgrader.h"

int main(int argc, char **argv)
{
    if ( argc != 14 )
    {
        // not called correctly
        printf("%d %d %5.2f %d", ZEXE_SYSERR, 0, 0.0, 0);
        return 0;
    }

    char* chroot_dest = argv[1];
    char* log_path    = argv[2];
    char* grader_path = argv[3];
    int   grader_uid  = atoi(argv[4]);
    int   grader_gid  = atoi(argv[5]);

    int r = chroot(chroot_dest);
    (void)r;

    init_grader(log_path);

    char* task_name         = argv[6];
    int   time_limit        = atoi(argv[7]);
    int   mem_limit         = atoi(argv[8]);
    int   out_limit         = atoi(argv[9]);
    int   test_case         = atoi(argv[10]);
    int   language          = atoi(argv[11]);
    char* tasks_path        = argv[12];
    int   is_interactive    = atoi(argv[13]);

    double time;
    int    memory;
    int    score;

    int result = perform_grade(grader_path,
                               grader_uid,
                               grader_gid,
                               grader_uid,
                               grader_gid,
                               tasks_path,
                               task_name,
                               test_case,
                               is_interactive,
                               language,
                               time_limit,
                               mem_limit,
                               out_limit,
                               &score,
                               &time,
                               &memory);

    printf("%d %d %5.2f %d", result, score, time, memory);

    post_log();

    return 0;
}
