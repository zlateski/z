/*
 * Copyright (c) 2009-2012 z-training -- http://z-training.net
 *
 * AUTHORS:
 *   Aleksandar Zlateski (admin@z-trening.com, z@zlateski.com)
 *
 */

#include <stdio.h>

#include "zgrader/zgrader.h"

int main(int argc, char** argv)
{

    if ( argc != 9 )
    {
        // not called correctly
        printf("%d %5.2f %s", ZEXE_SYSERR, 0.0f, "Compiler called with wrong arguments\n");
        return 0;
    }

    char* chroot_dest = argv[1];
    char* log_path    = argv[2];
    char* grader_path = argv[3];
    int   grader_uid  = atoi(argv[4]);
    int   grader_gid  = atoi(argv[5]);

    int r = chroot(chroot_dest);
    (void)r;
    init_grader(log_path);

    char* source_file    = argv[6];
    int   language       = atoi(argv[7]);
    int   time_limit     = atoi(argv[8]);

    double time;
    char   compile_result[1000001];

    int    result = perform_compile(grader_path,
                                    grader_uid,
                                    grader_gid,
                                    source_file,
                                    language,
                                    time_limit,
                                    &time,
                                    compile_result);

    printf("%d %5.2f %s", result, time, compile_result);

    post_log();

    return 0;
}
