/*
 * Copyright (c) 2009-2012 z-training -- http://z-training.net
 *
 * AUTHORS:
 *   Aleksandar Zlateski (admin@z-trening.com, z@zlateski.com)
 *
 */

#include "zgrader.h"

/**
 * LOOK AT int execute(...) FOR REFERENCE. THE TWO FUNCTIONS ARE
 * VERY SIMILAR.
 */
int compile(char *const exec_arv[],
            const char* sandbox,
            const char *source,
            const char *dest,
            const char *err_file,
            const exec_params_st* exec_params,
            exec_results_st* _exit_result)
{

    LOGGER("compile (%s : %s => %s) :: limits:%d",
           sandbox, source, dest, exec_params->time_l);

    fd_t  fd_in, fd_err, fd_res;
    pid_t pid_run;

    IFNEG_RET(fd_in  = z_open2("/dev/null", O_RDONLY), ZEXE_SYSERR);
    IFNEG_RET(fd_err = get_collect(err_file),          ZEXE_SYSERR);

    IFNEG_RET((pid_run = subprocess_run(sandbox, exec_arv, source,
                                        fd_in, fd_err, fd_err,
                                        false, exec_params,
                                        &fd_res)), ZEXE_SYSERR);

    IFNEG_RET(z_close(fd_in),  ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_err), ZEXE_SYSERR);

    // wait for all
    // ---------------
    // - collector pid_err (reading the compile stdout and stderr)
    // - runner    pid_run

    bool run_healthy = true;

    int st;
    while (true) {
        pid_t child = waitpid(-1, &st, 0);
        if (child < 0) {
            if (errno == ECHILD)
                break;
            return ZEXE_SYSERR;
        }

        __DEBUG("compile (%s : %s => %s) :: WAITING - "
                "GOT SIGNAL FROM %d: sig %d code %d ",
                sandbox, source, dest, child, WTERMSIG(st), WEXITSTATUS(st));

        if (child == pid_run) {
            // exited is okay! signaled is not!
            if (WIFSIGNALED(st) || (WIFEXITED(st) && WEXITSTATUS(st))) {
                run_healthy = false;
                __DEBUG("compile (%s : %s => %s) :: WAITING - "
                        "THAT WAS A TERMSIG OR EC !=0 FROM THE RUNNER!",
                        sandbox, source, dest);
                if (WIFSIGNALED(st) && (WTERMSIG(st) == SIGZERR)) {
                    __DEBUG("compile (%s : %s => %s) :: THAT WAS A TERMSIG __SIGZERR__ - "
                            "LOOK AT zgrader_types.h CODE: EX__MAX + %d",
                            sandbox, source, dest, (WEXITSTATUS(st) - EX__MAX));
                }
            }
        }

    }

    // if not healthy report system error, but close the file descriptor
    if (!run_healthy) {
        IFNEG_RET(z_close(fd_res), ZEXE_SYSERR);
        return ZEXE_SYSERR;
    }

    // get result status
    IFNEG_RET(read_exec_results(fd_res, exec_params, _exit_result), ZEXE_SYSERR);

    LOGGER("compile (%s : %s => %s) :: signal : %d, exit : %d",
           sandbox, source, dest, _exit_result->exit_signal,
           _exit_result->exit_code);

    return 0;
}

