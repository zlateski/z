/*
 * Copyright (c) 2009-2012 z-training -- http://z-training.net
 *
 * AUTHORS:
 *   Aleksandar Zlateski (admin@z-trening.com, z@zlateski.com)
 *
 */

#ifndef ZI_ZGRADER_CONSTANTS_H_
#define ZI_ZGRADER_CONSTANTS_H_

#define PROBLEM_EXEC   "problem.compiled"
#define PROBLEM_EXEC2  "problem.compiled2"
#define IN_FILE        "problem.in"
#define OUT_FILE       "problem.out"
#define SOL_FILE       "problem.sol"
#define CHECKER_FILE   "checker.compiled"
#define BUMPER_FILE    "bumper.compiled"
#define JAVA_CLASS     "zi"

#define ERR_FILE       "problem.err"
#define CHECKER_OUT    "checker.out"
#define CHECKER_ERR    "checker.err"

#define fd_t           int
#define rlimit_st      struct rlimit
#define rusage_st      struct rusage
#define stat_st        struct stat
#define zlang_t        int

/* errors */

#define ZSBXUNKNOWN           0xf0000000
#define ZSBXFFORK             0xffffffff
#define ZSBXFDUP2             0xfffffff8
#define ZSBXFRLIMIT           0xfffffff4
#define ZSBXFCHDIR            0xfffffff2
#define ZSBXFCHOWN            0xfffffff1
#define ZSBXFCHMOD            0xfffffff0
#define ZSBXFCHROOT           0xffffff80
#define ZSBXFSUID             0xffffff40
#define ZSBXFSGID             0xffffff20
#define ZSBXFPTRACEME         0xffffff10
#define ZSBXFEXEC             0xffffff00
#define ZSBXFCLOSE            0xfffff800
#define ZSBXFWAIT4            0xfffff400
#define ZSBXFPTCECONT         0xfffff200
#define ZSBXFGMEMUSG          0xfffff100
#define ZSBXFPTCEGENV         0xfffff000
#define ZSBXFSSGNL            0xffff8000
#define ZSBXFSITIMER          0xffff4000
#define ZSBXFPIPE             0xffff2000
#define ZSBXFFDOPEN           0xffff1000
#define ZSBXFFCLOSE           0xffff0000

/* return codes */

#define ZSBXREARLYEXIT        0x00000010
#define ZSBXRNOTPTRACED       0x00000020


/* prog langs */
#define ZLANG_FPC             0x00010001
#define ZLANG_C               0x00010002
#define ZLANG_CPP             0x00010003
#define ZLANG_JAVA            0x01010004
#define ZLANG_PYTHON          0x01000005
#define ZLANG_PHP             0x01000006
#define ZLANG_PERL            0x01000007

#define ZLANGCOMPILES(x)      (x & 0x0001000)
#define ZLANGINTERPRETS(x)    (x & 0x0100000)

#define ZEXE_SYSERR           0xfffffffa
#define ZEXE_TLE              0xffffffff
#define ZEXE_RTE              0xfffffffe
#define ZEXE_MLE              0xfffffffd
#define ZEXE_OLE              0xfffffffc
#define ZEXE_NOT0             0xfffffffb

#endif
