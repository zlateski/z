/*
 * Copyright (c) 2009-2012 z-training -- http://z-training.net
 *
 * AUTHORS:
 *   Aleksandar Zlateski (admin@z-trening.com, z@zlateski.com)
 *
 */

#ifndef ZI_ZGRADER_MACROS_H_
#define ZI_ZGRADER_MACROS_H_

#ifndef NDEBUG
#  define DEBUG_MODE
#endif

#define MIN(a,b)                                \
    ((a)<(b)?(a):(b))

#define MAX(a,b)                                \
    ((a)<(b)?(b):(a))

#ifndef DEBUG_MODE
#define __DEBUG(...)                            \
    do {} while(0)
#else
#define __DEBUG(...)                            \
    do {                                        \
        fprintf(logger_f,                       \
                "__DEBUG  (PID: %d) ",          \
                getpid());                      \
        logger(__VA_ARGS__);                    \
    } while(0)
#endif

#ifndef DEBUG_MODE
#define LOGGER(...)                             \
    do {                                        \
        logger(__VA_ARGS__);                    \
    } while(0)
#else
#define LOGGER(...)                             \
    do {                                        \
        fprintf(logger_f,                       \
                "__LOG    (PID: %d) ",          \
                getpid());                      \
        logger(__VA_ARGS__);                    \
    } while(0)
#endif


#define ITIMERVALSET(time_limit, RET_ERR)                               \
    do {                                                                \
        struct itimerval name;                                          \
        name.it_interval.tv_sec = name.it_interval.tv_usec = 0;         \
        name.it_value.tv_sec = (time_limit) / 1000;                     \
        name.it_value.tv_usec = 1000 * ((time_limit) % 1000);           \
        LOGGER(">>settimer (%d, %d)", name.it_value.tv_sec,             \
               name.it_value.tv_usec);                                  \
        IFNEG_RET(setitimer(ITIMER_REAL, &name, NULL), RET_ERR);        \
    } while(0)

#define LOG_AND_RET(text, code)                                 \
    do {                                                        \
        fprintf(logger_f,                                       \
                "__ERROR  (PID: %d) ",                          \
                getpid());                                      \
        logger("%s:%u (%s) : %s\n",                             \
               __FILE__, __LINE__, #text, strerror(errno));     \
        kill_children();                                        \
        return code;                                            \
    } while(0)

#define LOGCMP_AND_RET(text, result, correct, code)             \
    do {                                                        \
        fprintf(logger_f,                                       \
                "__ERROR  (PID: %d) ",                          \
                getpid());                                      \
        logger("%s:%u (%s) : %s (%ld ? %ld)\n",                 \
               __FILE__, __LINE__, #text, strerror(errno),      \
               (long int)result, (long int)correct);            \
        kill_children();                                        \
        return code;                                            \
    } while(0)

#define LOG_AND_EXIT(text, code)                                \
    do {                                                        \
        fprintf(logger_f,                                       \
                "__ERROR  (PID: %d) ",                          \
                getpid());                                      \
        logger("%s:%u (%s) : %s\n",                             \
               __FILE__, __LINE__, #text, strerror(errno));     \
        kill_children();                                        \
        exit(code);                                             \
    } while(0)


#define IFNEG_RET(try, code)                    \
    do {                                        \
        if ((try) < 0) {                        \
            LOG_AND_RET(try < 0, code);         \
        }                                       \
    } while(0)

#define IFLTE_RET(try, val, code)               \
    do {                                        \
        if ((try) <= val) {                     \
            LOG_AND_RET(#try <= #val, code);    \
        }                                       \
    } while(0)

#define IF_RET(try, code)                       \
    do {                                        \
        if ((try)) {                            \
            LOG_AND_RET(#try, code);            \
        }                                       \
    } while(0)

#define IFNEQ_RET(try, correct, code)                   \
    do {                                                \
        typeof(correct) r = (try);                      \
        if (r != correct) {                             \
            LOGCMP_AND_RET(#try, r, correct, code);     \
        }                                               \
    } while(0)

#define IFEQ_RET(try, correct, code)            \
    do {                                        \
        if ((try) == correct) {                 \
            LOG_AND_RET(#try == correct, code); \
        }                                       \
    } while(0)


#define IFNEG_EXIT(try, code)                   \
    do {                                        \
        if ((try) < 0) {                        \
            LOG_AND_EXIT(#try < 0, code);       \
        }                                       \
    } while(0)


#define IFLTE_EXIT(try, val, code)              \
    do {                                        \
        if ((try) <= val) {                     \
            LOG_AND_EXIT(#try <= #val, code);   \
        }                                       \
    } while(0)

#define IF_EXIT(try, code)                      \
    do {                                        \
        if ((try)) {                            \
            LOG_AND_EXIT(#try, code);           \
        }                                       \
    } while(0)

#define IFNEQ_EXIT(try, correct, code)                  \
    do {                                                \
        if ((try) != correct) {                         \
            LOG_AND_EXIT(#try != correct, code);        \
        }                                               \
    } while(0)

#define IFEQ_EXIT(try, correct, code)                   \
    do {                                                \
        if ((try) == correct) {                         \
            LOG_AND_EXIT(#try == correct, code);        \
        }                                               \
    } while(0)

#define LIST_FIND(var, head, field, val, vfld)  \
    do {                                        \
        LIST_FOREACH(var, head, field) {        \
            if ((var)->vfld == (val)) break;    \
        }                                       \
    } while (0)

#define FD_LIST_INSERT(fd_val)                                  \
    do {                                                        \
        fd_t_le *fl = (fd_t_le*)malloc(sizeof(fd_t_le));        \
        fl->fd = fd_val;                                        \
        LIST_INSERT_HEAD(&fd_list, fl, links);                  \
    } while(0)

#define PID_LIST_INSERT(pid_val)                                \
    do {                                                        \
        pid_t_le *pl = (pid_t_le*)malloc(sizeof(pid_t_le));     \
        pl->pid = pid_val;                                      \
        LIST_INSERT_HEAD(&pid_list, pl, links);                 \
    } while(0)

#define FD_LIST_REMOVE(fd_val)                          \
    do {                                                \
        fd_t_le *fl;                                    \
        LIST_FIND(fl, &fd_list, links, fd_no, fd);      \
        LIST_REMOVE(fl, links);                         \
    } while(0)

#define PID_LIST_REMOVE(pid_val)                        \
    do {                                                \
        pid_t_le *pl;                                   \
        LIST_FIND(pl, &pid_list, links, pid_no, pid);   \
        LIST_REMOVE(pl, links);                         \
    } while(0)

#define LIST_REMOVE_ALL(head, field)                            \
    do {                                                        \
        while (!LIST_EMPTY(head)) {                             \
            typeof(LIST_FIRST(head)) lf = LIST_FIRST(head);     \
            LIST_REMOVE(lf, field);                             \
            free(lf);                                           \
        }                                                       \
    } while(0)

#define PRINT_EXECV(file, execv)                \
    do {                                        \
        int i;                                  \
        for (i = 0; execv[i]; ++i)              \
            fprintf(file, "%s ", execv[i]);     \
        fprintf(file, "\n");                    \
        fflush(file);                           \
    } while(0)


#define STRCPY(target, source)                          \
    do {                                                \
        target = (char*)malloc(strlen(source) + 1);     \
        strcpy(target, source);                         \
    } while(0)

#define STRPRT(target, format, ...)             \
    do {                                        \
        char tmp[1024];                         \
        sprintf(tmp, format, __VA_ARGS__);      \
        STRCPY(target, tmp);                    \
    } while(0)


#define FTOK(_proj_id_)                         \
    ftok(__FILE__, _proj_id_)

#define SEM_ACQUIRE()                           \
    do {                                        \
        struct sembuf sops[2];                  \
        sops[0].sem_num = 0;                    \
        sops[0].sem_op  = 0;                    \
        sops[0].sem_flg = SEM_UNDO;             \
        sops[1].sem_num = 0;                    \
        sops[1].sem_op  = 1;                    \
        sops[1].sem_flg = SEM_UNDO;             \
        IFNEG_EXIT(semop(fmutex, sops, 2), 2);  \
    } while(0)

#define SEM_RELEASE()                           \
    do {                                        \
        struct sembuf sops[1];                  \
        sops[0].sem_num = 0;                    \
        sops[0].sem_op  = -1;                   \
        sops[0].sem_flg = SEM_UNDO;             \
        IFNEG_EXIT(semop(fmutex, sops, 1), 2);  \
    } while(0)



#endif
