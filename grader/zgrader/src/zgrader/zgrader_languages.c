/*
 * Copyright (c) 2009-2013 z-trening -- http://z-trening.com
 * AUTHORS:
 *   Aleksandar Zlateski (aleksandar.zlateski@gmail.com)
 *
 */

#include "zgrader_languages.h"

bool get_run_command(lang_t lang,
                     int time_l, int mem_l,
                     const char* compiled,
                     char *_command[],
                     bool *_chroot_l,
                     int *_time_l,
                     int *_mem_l,
                     int *_proc_l)
{
  switch (lang) {
  case ZLANG_FPC:
  case ZLANG_C:
  case ZLANG_CPP:
  case ZLANG_FORTRAN:
  case ZLANG_CPP11:
    *_time_l   = time_l;
    *_chroot_l = true;
    *_mem_l    = mem_l;
    *_proc_l   = 10;
    STRCPY(_command[0], compiled);
    _command[1] = NULL;
    return true;

  case ZLANG_GPC:
    *_time_l   = time_l;
    *_chroot_l = false;
    *_mem_l    = mem_l;
    *_proc_l   = 10;
    STRCPY(_command[0], compiled);
    _command[1] = NULL;
    return true;

  case ZLANG_JAVA:
    *_time_l    = 200 + (time_l * 3) / 2;
    *_chroot_l = false;
    *_mem_l    = 0;
    *_proc_l   = 20;
    _command[0] = "/usr/bin/java";
    _command[1] = "-Djava.security.manager";
    STRPRT(_command[2], "-Xmx%dm", (mem_l / (1024*1024)) + 1);
    //STRCPY(_command[3], compiled);
    _command[3] = "zi";
    _command[4] = NULL;
    return true;

  case ZLANG_CS:
    *_time_l    = 200 + (time_l * 3) / 2;
    *_chroot_l = false;
    *_mem_l    = 0;
    *_proc_l   = 20;
    _command[0] = "/usr/bin/gmcs";
    _command[1] = "-o+";
    _command[2] = "zi.cs";
    _command[3] = NULL;
    return true;

  case ZLANG_PYTHON:
    *_time_l    = time_l * 2;
    *_chroot_l = false;
    *_mem_l    = mem_l + 20*1024*1024;
    *_proc_l   = 20;
    _command[0] = "/usr/bin/python";
    STRCPY(_command[1], compiled);
    _command[2] = NULL;
    return true;


  case ZLANG_PERL:
    *_time_l    = time_l * 2;
    *_chroot_l = false;
    *_mem_l    = mem_l + 20*1024*1024;
    *_proc_l   = 20;
    _command[0] = "/usr/bin/perl";
    _command[1] = (char*)malloc(strlen(compiled));
    strcpy(_command[1], compiled);
    _command[2] = NULL;
    return true;

  case ZLANG_PHP:
    *_time_l    = time_l * 2;
    *_chroot_l = false;
    *_mem_l    = mem_l + 256*1024*1024;;
    *_proc_l   = 20;
    _command[0] = "/usr/bin/php";
    _command[1] = (char*)malloc(strlen(compiled));
    strcpy(_command[1], compiled);
    _command[2] = NULL;
    return true;

  case ZLANG_RUBY:
    *_time_l    = time_l * 2;
    *_chroot_l = false;
    *_mem_l    = mem_l + 20*1024*1024;;
    *_proc_l   = 20;
    _command[0] = "/usr/bin/ruby";
    _command[1] = (char*)malloc(strlen(compiled));
    strcpy(_command[1], compiled);
    _command[2] = NULL;
    return true;

  case ZLANG_SCHEME:
    *_time_l    = time_l * 2;
    *_chroot_l = false;
    *_mem_l    = mem_l + 256*1024*1024;;
    *_proc_l   = 20;
    _command[0] = "/usr/bin/guile-1.6";
    _command[1] = "-s";
    _command[2] = (char*)malloc(strlen(compiled));
    strcpy(_command[2], compiled);
    _command[3] = NULL;
    return true;

  case ZLANG_YBASIC:
    *_time_l    = time_l * 2;
    *_chroot_l = false;
    *_mem_l    = mem_l + 128*1024*1024;;
    *_proc_l   = 20;
    _command[0] = "/usr/bin/yabasic";
    _command[1] = (char*)malloc(strlen(compiled));
    strcpy(_command[1], compiled);
    _command[2] = NULL;
    return true;

  case ZLANG_MATLAB:
    *_time_l    = time_l * 2;
    *_chroot_l = false;
    *_mem_l    = mem_l + 128*1024*1024;;
    *_proc_l   = 20;
    _command[0] = "/usr/bin/octave";
    _command[1] = "-f";
    _command[2] = "-q";
    _command[3] = (char*)malloc(strlen(compiled));
    strcpy(_command[3], compiled);
    _command[4] = NULL;
    return true;

  default:
    return false;
  }
  return false;
}

bool get_compile_command(lang_t lang,
                         int time_l,
                         const char* source,
                         const char* executable,
                         char *_command[],
                         int *_time_l)
{
  switch (lang) {
  case ZLANG_FPC:
    *_time_l    = time_l;
    _command[0] = "/usr/bin/fpc";
    _command[1] = "-So";
    _command[2] = "-O2";
    _command[3] = "-XS";
    STRPRT(_command[4], "-o%s", executable);
    STRCPY(_command[5], source);
    _command[6] = NULL;
    return true;

  case ZLANG_C:
    *_time_l    = time_l;
    _command[0] = "/usr/bin/g++";
    _command[1] = "-O2";
    _command[2] = "-falign-jumps";
    _command[3] = "-falign-loops";
    _command[4] = "-falign-labels";
    _command[5] = "-o";
    STRCPY(_command[6], executable);
    _command[7] = "-x";
    _command[8] = "c";
    STRCPY(_command[9], source);
    _command[10] = "-lm";
    _command[11] = "-static";
    _command[12] = NULL;
    return true;

  case ZLANG_CPP:
    *_time_l    = time_l;
    _command[0] = "/usr/bin/g++";
    _command[1] = "-O2";
    _command[2] = "-falign-jumps";
    _command[3] = "-falign-loops";
    _command[4] = "-falign-labels";
    _command[5] = "-o";
    STRCPY(_command[6], executable);
    _command[7] = "-x";
    _command[8] = "c++";
    STRCPY(_command[9], source);
    _command[10] = "-lm";
    _command[11] = "-static";
    _command[12] = NULL;
    return true;

  case ZLANG_CPP11:
    *_time_l    = time_l;
    _command[0] = "/usr/bin/g++";
    _command[1] = "-O2";
    _command[2] = "-falign-jumps";
    _command[3] = "-falign-loops";
    _command[4] = "--std=c++11";
    _command[5] = "-o";
    STRCPY(_command[6], executable);
    _command[7] = "-x";
    _command[8] = "c++";
    STRCPY(_command[9], source);
    _command[10] = "-lm";
    _command[11] = "-static";
    _command[12] = NULL;
    return true;

  case ZLANG_JAVA:
    *_time_l    = time_l;
    _command[0] = "/usr/bin/javac";
    _command[1] = "-g:none"; // "-1.6" ??
    _command[2] = "-cp";
    _command[3] = "./";
    _command[4] = "-bootclasspath";
    _command[5] = JAVA_CLASS_PATH;
    STRCPY(_command[6], source);
    _command[7] = NULL;
    return true;

  case ZLANG_CS:
    *_time_l    = time_l;
    _command[0] = "/usr/bin/mono";
    STRCPY(_command[1], executable);
    _command[2] = NULL;
    return true;

  case ZLANG_FORTRAN:
    *_time_l    = time_l;
    _command[0] = "/usr/bin/gfortran";
    _command[1] = "-O2";
    _command[2] = "-o";
    STRCPY(_command[3], executable);
    STRCPY(_command[4], source);
    _command[5] = "-lm";
    _command[6] = "-static";
    _command[7] = NULL;
    return true;

  case ZLANG_PYTHON:
  case ZLANG_PERL:
  case ZLANG_PHP:
  case ZLANG_RUBY:
  case ZLANG_SCHEME:
  case ZLANG_YBASIC:
  case ZLANG_MATLAB:
    *_time_l    = time_l;
    _command[0] = "/bin/cp";
    STRCPY(_command[1], source);
    STRCPY(_command[2], executable);
    _command[3] = NULL;
    return true;

  case ZLANG_GPC:
    *_time_l    = time_l;
    _command[0] = "/usr/bin/gpc";
    _command[1] = "-O2";
    _command[2] = "-o";
    STRCPY(_command[3], executable);
    STRCPY(_command[4], source);
    _command[5] = "-lm";
    _command[6] = NULL;
    return true;

  default:
    return false;
  }
  return false;
}

bool get_source_bin_names(lang_t lang,
                          char* source,
                          char* executable)
{
  switch (lang) {
  case ZLANG_FPC:
    sprintf(source, "%s", "zi.p");
    sprintf(executable, "%s", "zi.bin");
    return true;

  case ZLANG_GPC:
    sprintf(source, "%s", "zi.p");
    sprintf(executable, "%s", "zi.bin");
    return true;

  case ZLANG_C:
    sprintf(source, "%s", "zi.c");
    sprintf(executable, "%s", "zi.bin");
    return true;

  case ZLANG_CPP:
    sprintf(source, "%s", "zi.cpp");
    sprintf(executable, "%s", "zi.bin");
    return true;

  case ZLANG_CPP11:
    sprintf(source, "%s", "zi.cpp11");
    sprintf(executable, "%s", "zi.bin");
    return true;

  case ZLANG_JAVA:
    sprintf(source, "%s", "zi.java");
    sprintf(executable, "%s", "zi.class");
    return true;

  case ZLANG_CS:
    sprintf(source, "%s", "zi.cs");
    sprintf(executable, "%s", "zi.exe");
    return true;

  case ZLANG_PYTHON:
    sprintf(source, "%s", "zi.pypc");
    sprintf(executable, "%s", "zi.py");
    return true;

  case ZLANG_PERL:
    sprintf(source, "%s", "zi.plpc");
    sprintf(executable, "%s", "zi.pl");
    return true;

  case ZLANG_PHP:
    sprintf(source, "%s", "zi.phpx");
    sprintf(executable, "%s", "zi.php");
    return true;

  case ZLANG_RUBY:
    sprintf(source, "%s", "zi.rbpc");
    sprintf(executable, "%s", "zi.rb");
    return true;

  case ZLANG_SCHEME:
    sprintf(source, "%s", "zi.scmx");
    sprintf(executable, "%s", "zi.scm");
    return true;

  case ZLANG_FORTRAN:
    sprintf(source, "%s", "zi.f");
    sprintf(executable, "%s", "zi.bin");
    return true;

  case ZLANG_YBASIC:
    sprintf(source, "%s", "zi.basx");
    sprintf(executable, "%s", "zi.bas");
    return true;

  case ZLANG_MATLAB:
    sprintf(source, "%s", "zi.mx");
    sprintf(executable, "%s", "zi.m");
    return true;

  default:
    return false;
  }
  return false;
}
