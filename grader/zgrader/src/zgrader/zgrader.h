/*
 * Copyright (c) 2009-2010 z-training -- http://z-training.net
 * AUTHORS:
 *   Aleksandar Zlateski (admin@z-trening.com, z@zlateski.com)
 *
 */

#ifndef ZI_ZGRADER_H_
#define ZI_ZGRADER_H_

#ifdef  __cplusplus
extern "C" {
#endif

#include "zgrader_types.h"
#include "zgrader_macros.h"
#include "zgrader_languages.h"

#define LOGGER_FILE "./logger.log"
#ifdef DEBUG_MODE
extern char log_spaces[10];
extern int  log_offset;
#endif

/* zgrader_utility.c */
extern FILE*    logger_f;
int   init_grader(    const char *log_file);
void  logger(         const char *format, ...);
int   get_mem_usage(  pid_t pid);
fd_t  get_feeder(     const char* file_in);
fd_t  get_collect(    const char* file_out);
int   remove_dir(     const char* dir_path);
int   copy_file(      const char* source, const char* dest, mode_t mode);
int   prepare_sandbox(const char* path);
int   unlock_sandbox( const char* path);
int   lock_sandbox(   const char* path);
int   post_log();
int   get_file_cont(  const char* file_path,
                      char* buffer,
                      int max_len);

/* zgrader_sys.c */
extern fd_t_lh  fd_list;
extern pid_t_lh pid_list;
extern pid_t    run_pid;
void  alarm_killall(  int signo);
void  alarm_handler(  int signo);
pid_t z_fork(         int *pipes, int p_count);
int   z_pipe(         fd_t* pipes);
int   z_close(        int fd);
fd_t  z_open2(        const char *pathname, int flags);
fd_t  z_open3(        const char *pathname, int flags, mode_t mode);
FILE* z_fdopen(       fd_t fd, const char* mode);
FILE* z_fclose(       FILE *f);
void  killall(        uid_t uid);
void  force_clean();
void  close_pipes();
void  kill_children();

/* zgrader_execute.c */
int execute(           char *const exec_arv[],
                       const char *sandbox,
                       const char* exec_file,
                       const char *in_file,
                       const char *out_file,
                       const char *err_file,
                       bool chrooted,
                       const exec_params_st* exec_params,
                       exec_results_st* _exit_result);

pid_t subprocess_run(  const char* sandbox,
                       char *const exec_arv[],
                       const char* exec_file,
                       fd_t fd_in,
                       fd_t fd_out,
                       fd_t fd_err,
                       bool chrted,
                       const exec_params_st* exec_params,
                       fd_t *_res_pipe);

int run_sandboxed(     const char* sandbox,
                       char *const exec_arv[],
                       const char* exec_file,
                       fd_t fd_in,
                       fd_t fd_out,
                       fd_t fd_err,
                       bool chrted,
                       const exec_params_st* exec_params,
                       double* _time,
                       int* _memory,
                       int* _status);

int get_exec_result(   int status,
                       const exec_params_st* exec_params,
                       const exec_results_st* exec_result);

int read_exec_results( fd_t fd_res,
                       const exec_params_st* exec_params,
                       exec_results_st* _exec_result);

int apply_exec_params( const exec_params_st* exec_params);

/* zgrader_compile.c */
int compile(           char *const exec_arv[],
                       const char* sandbox,
                       const char *source,
                       const char *dest,
                       const char *err_file,
                       const exec_params_st* exec_params,
                       exec_results_st* _exit_result);

/* zgrader_checking.c */
int check(             const char* sandbox,
                       const char* checker_bin,
                       const char* in_file,
                       const char* out_file,
                       const char* sol_file,
                       const char* err_file,
                       const exec_params_st* exec_params,
                       exec_results_st* _exit_result,
                       int* _pts);

int interactive(       char *const exec_arv[],
                       const char* sandbox,
                       const char* exec_file,
                       const char* checker_bin,
                       const char* in_file,
                       const char* sol_file,
                       const char* eerr_file,
                       const char* cerr_file,
                       bool chrooted,
                       const exec_params_st* exec_params,
                       const exec_params_st* cexec_params,
                       exec_results_st* _exit_result,
                       exec_results_st* _cexit_result,
                       int* _pts);


int read_score(        fd_t fd_score,
                       int* _pts);
/* zgrader_public.c */
int perform_compile(   const char* sandbox_path,
                       uid_t grader_uid,
                       gid_t grader_gid,
                       const char* source_file,
                       int lang,
                       int time_limit,
                       double* _time,
                       char* _output);

int perform_grade(     const char* sandbox_path,
                       uid_t grader_uid,
                       gid_t grader_gid,
                       uid_t checker_uid,
                       gid_t checker_gid,
                       const char* task_path,
                       const char* task_name,
                       int test_no,
                       bool is_interactive,
                       lang_t lang,
                       int time_limit,
                       int mem_limit,
                       int out_limit,
                       int* _score,
                       double* _time,
                       int* _memory);

#ifdef  __cplusplus
}
#endif


#endif /*ZI_GRADER_H_*/
