/*
 * Copyright (c) 2009-2012 z-training -- http://z-training.net
 * AUTHORS:
 *   Aleksandar Zlateski (admin@z-trening.com, z@zlateski.com)
 *
 */

#include "zgrader.h"

fd_t_lh  fd_list;
pid_t_lh pid_list;
pid_t    run_pid;

//TODO: little bit nicer!
void  killall(uid_t uid) {
    __DEBUG(">>killall (%d) :: KILLING ALL", uid);
    pid_t pid;
    if ((pid = fork()) == 0) {
        char suid[20];
        sprintf(suid, "%d", uid);
        execl("/usr/bin/pkill", "/usr/bin/pkill", "-9", "-U", suid, NULL);
    }
    int st;
    int res = waitpid(pid, &st, 0);
    (void)res;
    __DEBUG(">>killall (%d) :: KILLING ALL RES: %d, %d", uid, res, st);
}

void alarm_handler(int signo) {
    (void)signo;
    __DEBUG("Alarm killall called with signal %d from %d",
            signo, getpid());
    __DEBUG("Killing %d with signal %d", run_pid, SIGKILL);
    kill(run_pid, SIGKILL);
}

void alarm_killall(int signo) {
    LOGGER("Alarm killall called with signal %d from %d",
           signo, getpid());
    if (signo == SIGALRM) {
        kill_children();
    }
}

void force_clean()
{
    kill_children();
    close_pipes();
}

void kill_children()
{
    pid_t_le *le;
    LIST_FOREACH(le, &pid_list, links) {
        int result = kill(le->pid, SIGKILL);
        (void)result;
        __DEBUG("_____ killing %d: result %d", le->pid,  result);
    }
    LIST_REMOVE_ALL(&pid_list, links);
}

void close_pipes()
{
    fd_t_le *le;
    LIST_FOREACH(le, &fd_list, links) {
        int result = close(le->fd);
        (void)result;
        __DEBUG("_____ closing %d: result %d", le->fd, result);
    }
    LIST_REMOVE_ALL(&fd_list, links);
}

pid_t z_wait_pid(pid_t pid, int *status, int options)
{
    pid_t child = waitpid(pid, status, options);
    if (child > 0) {

    }
    return child;
}

pid_t z_fork(int *pipes, int p_count)
{
    pid_t pid = fork();

    if (pid == 0) {

#ifdef DEBUG_MODE
        log_spaces[log_offset] = '\t';
        log_offset++ ;
        log_spaces[log_offset] = '\0';
#endif

        fd_t_lh fd_to_close;
        LIST_INIT(&fd_to_close);

        fd_t_le *le;

        LIST_FOREACH(le, &fd_list, links) {
            bool to_del = true;
            int i;
            for (i = 0; i < p_count; ++i) {
                if (le->fd == pipes[i]) {
                    to_del = false;
                    break;
                }
            }
            if (to_del) {
                LIST_INSERT_HEAD(&fd_to_close, le, links);
            }
        }

        LIST_FOREACH(le, &fd_to_close, links) {
            IFNEG_EXIT(close(le->fd), -1); //TODO: ret value not -1 ?
            LIST_REMOVE(le, links);
        }

        LIST_REMOVE_ALL(&fd_to_close, links);

        return pid;
    }

    if (pid > 0) {
        PID_LIST_INSERT(pid);
        return pid;
    }

    return pid;
}

int z_pipe(fd_t* pipes)
{
    int res = pipe(pipes);
    if (res == 0) {
        FD_LIST_INSERT(pipes[0]);
        FD_LIST_INSERT(pipes[1]);
    }
    return res;
}

int z_close(int fd_no)
{
    __DEBUG(">> z_close(%d) from %d called", fd_no, getpid());
    int res = close(fd_no);
    if (res == 0) {
        FD_LIST_REMOVE(res);
    }
    return res;
}

fd_t z_open2(const char *pathname, int flags)
{
    int res = open(pathname, flags);
    if (res != -1) {
        FD_LIST_INSERT(res);
    }
    return res;
}

fd_t z_open3(const char *pathname, int flags, mode_t mode)
{
    __DEBUG(">> z_open3(%s) from %d called", pathname, getpid());
    int res = open(pathname, flags, mode);
    if (res != -1) {
        FD_LIST_INSERT(res);
    }
    return res;
}

FILE* z_fdopen(fd_t fd, const char* mode)
{
    FILE *f = fdopen(fd, mode);
    return f;
}

FILE* z_fclose(FILE* f)
{
    (void)f;
    //@todo: close the file's descriptor!
    return NULL;
}
