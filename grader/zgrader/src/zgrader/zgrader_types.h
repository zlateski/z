/*
 * Copyright (c) 2009-2010 z-training -- http://z-training.net
 * AUTHORS:
 *   Aleksandar Zlateski (admin@z-trening.com, z@zlateski.com)
 *
 */

#ifndef ZI_ZGRADER_TYPES_H_
#define ZI_ZGRADER_TYPES_H_

#include <sys/queue.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <sys/ptrace.h>
#include <sys/wait.h>
#include <sys/times.h>
#include <sys/time.h>
#include <stdlib.h>
#include <sysexits.h>
#include <unistd.h>
#include <limits.h>
#include <stdio.h>
#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <signal.h>
#include <stdarg.h>
#include <math.h>
#include <string.h>
#include <time.h>

#define bool  int
#define true  1
#define false 0

#define PROBLEM_EXEC   "problem.compiled"
#define PROBLEM_EXEC2  "problem.compiled2"
#define IN_FILE        "problem.in"
#define OUT_FILE       "problem.out"
#define SOL_FILE       "problem.sol"
#define CHECKER_FILE   "checker.compiled"
#define BUMPER_FILE    "bumper.compiled"
#define JAVA_CLASS     "zi"

#define ERR_FILE       "problem.err"
#define CHECKER_OUT    "checker.out"
#define CHECKER_ERR    "checker.err"

#define fd_t           int
#define rlimit_st      struct rlimit
#define rusage_st      struct rusage
#define stat_st        struct stat

/* we use the unused signal */
#define SIGZERR                SIGUNUSED

/* z errors codes */
#define ZSBXUNKNOWN            (EX__MAX + 1)   /* don't now what happened    */
#define ZSBXFFORK              (EX__MAX + 2)   /* fork failed                */
#define ZSBXFDUP2              (EX__MAX + 3)   /* dup2 failed                */
#define ZSBXFRLIMIT            (EX__MAX + 4)   /* rlimit failed              */
#define ZSBXFCHDIR             (EX__MAX + 5)   /* chdir failed               */
#define ZSBXFCHOWN             (EX__MAX + 6)   /* chown failed               */
#define ZSBXFCHMOD             (EX__MAX + 7)   /* chmod failed               */
#define ZSBXFCHROOT            (EX__MAX + 8)   /* chroot failed              */
#define ZSBXFSUID              (EX__MAX + 9)   /* setuid failed              */
#define ZSBXFSGID              (EX__MAX + 10)  /* setgid failed              */
#define ZSBXFPTRACEME          (EX__MAX + 11)  /* prace(TRACE_ME) failed     */
#define ZSBXFEXEC              (EX__MAX + 12)  /* exec failed                */
#define ZSBXFCLOSE             (EX__MAX + 13)  /* close filed                */
#define ZSBXFWAIT4             (EX__MAX + 14)  /* wait4 failed               */
#define ZSBXFPTCECONT          (EX__MAX + 15)  /* ptrace(TRACE_CONT) failed  */
#define ZSBXFGMEMUSG           (EX__MAX + 16)  /* get_mem_usage failed       */
#define ZSBXFPTCEGENV          (EX__MAX + 17)  /* ptrace(GET_ENV) failed     */
#define ZSBXFSSGNL             (EX__MAX + 18)  /* signal failed              */
#define ZSBXFSITIMER           (EX__MAX + 19)  /* setitimer failed           */
#define ZSBXFPIPE              (EX__MAX + 20)  /* pipe failed                */
#define ZSBXFFDOPEN            (EX__MAX + 21)  /* fdopen failed              */
#define ZSBXFFCLOSE            (EX__MAX + 22)  /* fclose failed              */
#define ZSBXREARLYEXIT         (EX__MAX + 23)  /* exited early               */
#define ZSBXRNOTPTRACED        (EX__MAX + 24)  /* escaped ptrace somehow     */
                                               /* or file not present        */

#define __ZSIG(err)            W_EXITCODE(err, SIGZERR)

#define ZEXE_SYSERR            0xfffffffa
#define ZEXE_TLE               0xffffffff
#define ZEXE_RTE               0xfffffffe
#define ZEXE_MLE               0xfffffffd
#define ZEXE_OLE               0xfffffffc
#define ZEXE_NOT0              0xfffffffb
#define ZEXE_CHKRERR           0xfffffff0

// compile limits
#define COMP_FERR_LMT 100000
#define COMP_TIME_LMT 60

// checker limits
#define CHKR_FERR_LMT 100000

struct exec_results_st_ {
  int    runner_result; // runner's result
  int    result;        // executable's result
  double time;          // in milliseconds
  int    memory;        // in KB
  int    status;
  int    exit_signal;
  int    exit_code;
};

#define exec_results_st struct exec_results_st_

struct exec_params_st_ {
  uid_t uid;            // uid of the process
  gid_t gid;            // gid of the process
  int   time_l;         // time limit in milliseconds
  int   mem_l;          // mem limit n bytes!
  int   out_l;          // data output limit in bytes
  int   nproc_l;        // #processess limit
};

#define exec_params_st struct exec_params_st_

#define _INITEXECPARAMS(name)                   \
  do {                                          \
    name.uid = 0;                               \
    name.gid = 0;                               \
    name.time_l = 0;                            \
    name.mem_l = 0;                             \
    name.out_l = 0;                             \
    name.nproc_l = 0;                           \
  } while(0)

struct fd_t_le_st {
  fd_t fd;
  LIST_ENTRY(fd_t_le_st) links;
};

#define fd_t_le struct fd_t_le_st

LIST_HEAD(fd_t_lh_st, fd_t_le_st);
#define fd_t_lh struct fd_t_lh_st

struct pid_t_le_st {
  pid_t pid;
  LIST_ENTRY(pid_t_le_st) links;
};
#define pid_t_le struct pid_t_le_st

LIST_HEAD(pid_t_lh_st, pid_t_le_st);
#define pid_t_lh struct pid_t_lh_st



#endif
