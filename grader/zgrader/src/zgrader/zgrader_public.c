/*
 * Copyright (c) 2009-2013 z-trening -- http://z-trening.com
 * AUTHORS:
 *
 *   Aleksandar Zlateski (aleksandar.zlateski@gmail.com)
 *
 */

#include "zgrader.h"

int perform_compile(   const char* sandbox_path,
                       uid_t grader_uid,
                       gid_t grader_gid,
                       const char* source_file,
                       int lang,
                       int time_limit,
                       double* _time,
                       char* _output)
{
    // kill all the processess, close all fdescriptors
    force_clean();

    // prepare the sandbox
    IFNEG_RET(prepare_sandbox(sandbox_path), -2);

    // get correct file names
    char src_n[1024];
    char bin_n[1024];
    IFNEG_RET(get_source_bin_names(lang, src_n, bin_n), -2);

    // and the corret paths
    char src_sbx[1024];
    char bin_sbx[1024];
    sprintf(src_sbx, "%s/%s", sandbox_path, src_n);
    sprintf(bin_sbx, "%s/%s", sandbox_path, bin_n);

    __DEBUG("LANG: %d, SOURCE FILE: %s, INSBX: %s", lang, source_file, src_sbx);

    // copy the source file
    IFNEG_RET(copy_file(source_file, src_sbx,
                        S_IRWXU | S_IRGRP | S_IROTH), -2);

    // and the error file name
    char cplerr[1024];
    sprintf(cplerr, "%s/compile.err", sandbox_path);

    // get the compile command
    int timel;
    char *execvv[20];
    IFNEG_RET(get_compile_command(lang, time_limit,
                                  src_sbx,
                                  bin_n, execvv, &timel), -2);

    // execution paramas
    exec_params_st ep;
    _INITEXECPARAMS(ep);

    ep.uid = grader_uid;
    ep.gid = grader_gid;
    ep.out_l = 10000000; // 10M of output
    ep.time_l = timel;

    exec_results_st er;
    int r = compile(execvv, sandbox_path, src_sbx,
                    bin_sbx, cplerr, &ep, &er);

    lock_sandbox(sandbox_path);
    // kill all user procs
    killall(grader_uid);

    if (r == 0) {
        *_time = er.time;
        IFNEG_RET(get_file_cont(cplerr, _output, 1000000), -2);
        if ((er.exit_code == 0) && (er.exit_signal == 0))
            return 0;
    }

    return -1;
}

int perform_grade(const char* sandbox_path,
                  uid_t grader_uid,
                  gid_t grader_gid,
                  uid_t checker_uid,
                  gid_t checker_gid,
                  const char* task_path,
                  const char* task_name,
                  int test_no,
                  bool is_interactive,
                  lang_t lang,
                  int time_limit,
                  int mem_limit,
                  int out_limit,
                  int* _score,
                  double* _time,
                  int* _memory)
{
    char *fin, *fout, *ferr, *fcrr, *fsol, *fchecker;
    STRPRT(fin,  "%s.%.2d.in",  task_name, test_no);
    STRPRT(fout, "%s.%.2d.out", task_name, test_no);
    STRPRT(fsol, "%s.%.2d.sol", task_name, test_no);
    STRPRT(ferr, "%s.%.2d.err", task_name, test_no);
    STRPRT(fcrr, "%s.%.2d.crr", task_name, test_no);
    STRPRT(fchecker, "%s.%.2d.checker", task_name, test_no);

    char *sfin, *sfsol, *sfchecker, *sferr, *sfcrr, *sfout;
    STRPRT(sfin,  "%s/%s",  sandbox_path, fin);
    STRPRT(sfout, "%s/%s",  sandbox_path, fout);
    STRPRT(sfsol, "%s/%s",  sandbox_path, fsol);
    STRPRT(sfcrr, "%s/%s",  sandbox_path, fcrr);
    STRPRT(sferr, "%s/%s",  sandbox_path, ferr);
    STRPRT(sfchecker, "%s/%s", sandbox_path, fchecker);

    char *orfin, *orfsol, *orfchecker;
    STRPRT(orfin,  "%s/%s/%s",  task_path, task_name, fin);
    STRPRT(orfsol, "%s/%s/%s",  task_path, task_name, fsol);
    STRPRT(orfchecker, "%s/%s/%s.checker", task_path, task_name, task_name);

    IFNEG_RET(copy_file(orfin, sfin,
                        S_IRWXU | S_IRGRP | S_IROTH), ZEXE_SYSERR);

    if (is_interactive) {
        // not fatal
        copy_file(orfsol, sfsol,
                  S_IRWXU | S_IRGRP | S_IROTH);
    }
    IFNEG_RET(copy_file(orfchecker, sfchecker,
                        S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH),
              ZEXE_SYSERR);

    // get correct file names
    char src_n[1024];
    char bin_n[1024];
    IFNEG_RET(get_source_bin_names(lang, src_n, bin_n), ZEXE_SYSERR);

    // and the corret paths
    char src_sbx[1024];
    char bin_sbx[1024];
    sprintf(src_sbx, "%s/%s", sandbox_path, src_n);
    sprintf(bin_sbx, "%s/%s", sandbox_path, bin_n);

    int pts, meml, ischrooted, procl, timel;
    char *execvv[20];

    (void)pts;
    IFNEG_RET(get_run_command(lang, time_limit,
                              mem_limit, bin_n,
                              execvv, &ischrooted,
                              &timel, &meml, &procl), ZEXE_SYSERR);

    // execution paramas
    exec_params_st ep;
    _INITEXECPARAMS(ep);

    ep.uid   = grader_uid;
    ep.gid   = grader_gid;
    ep.out_l = out_limit;
    ep.time_l  = timel;
    ep.mem_l   = meml;
    ep.nproc_l = procl;

    // execution paramas (checker)
    exec_params_st cep;
    _INITEXECPARAMS(cep);

    cep.uid   = checker_uid;
    cep.gid   = checker_gid;
    cep.out_l = out_limit;
    cep.time_l  = timel;
    cep.mem_l   = meml;
    cep.nproc_l = procl;

    // execution results
    exec_results_st er;
    exec_results_st cer;

    if (!is_interactive) {

        int r = execute(execvv,
                        sandbox_path,
                        bin_n,
                        sfin,
                        sfout, sferr,
                        ischrooted,
                        &ep, &er);
        killall(grader_uid);


        IFNEG_RET(copy_file(orfsol, sfsol, S_IRWXU | S_IRGRP | S_IROTH),
                  ZEXE_SYSERR);

        IFNEG_RET(copy_file(orfchecker, sfchecker, S_IRWXU | S_IRGRP | S_IROTH),
                  ZEXE_SYSERR);

        if (r == 0) {
            *_time = er.time;
            *_memory = er.memory;

            if (check(sandbox_path, fchecker, fin, fout, fsol, sfcrr,
                      &cep, &cer, _score) != 0) {
                *_score = 0;
            }
            killall(grader_uid);
            LOGGER("Done with checker!");
        }
        return r;
    } else {

        int r = interactive(execvv,
                            sandbox_path,
                            bin_n,
                            fchecker,
                            sfin,
                            sfsol,
                            sferr,
                            sfcrr,
                            ischrooted,
                            &ep, &cep,
                            &er, &cer,
                            _score);

        killall(grader_uid);

        *_time = er.time;
        *_memory = er.memory;
        if (r != 0) {
            *_score = 0;
        }

        return er.result;
    }
    return 0;
}
