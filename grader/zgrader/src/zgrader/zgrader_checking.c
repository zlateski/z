/*
 * Copyright (c) 2009-2010 z-training -- http://z-training.net
 * AUTHORS:
 *
 *   Aleksandar Zlateski (admin@z-trening.com, z@zlateski.com)
 *
 */

#include "zgrader.h"

/**
 * @NOTE: the function returns the points assigned by the checker
 *        but don't forget to check the exit signal and code!
 *        they must equal to 0, otherwise the checker did not
 *        run correctly!
 */
int check(const char* sandbox,
          const char* checker_bin,
          const char* in_file,
          const char* out_file,
          const char* sol_file,
          const char* err_file,
          const exec_params_st* exec_params,
          exec_results_st* _exit_result,
          int* _pts)
{

    LOGGER("check (%s : %s, %s, %s) :: limits:%d",
           sandbox, in_file, out_file, sol_file, exec_params->time_l);

    fd_t  fd_in, fd_err, fd_score[2], fd_res;
    // now we directly pipe the score to us
    IFNEG_RET(z_pipe(fd_score), ZEXE_SYSERR);
    pid_t pid_run;

    // kill the stdin for the checker
    IFNEG_RET(fd_in  = z_open2("/dev/null", O_RDONLY), ZEXE_SYSERR);
    IFNEG_RET(fd_err = get_collect(err_file),          ZEXE_SYSERR);

    // generate the arguments for the checker execution
    char *execvv[5];
    STRCPY(execvv[0], checker_bin);
    STRCPY(execvv[1], in_file);
    STRCPY(execvv[2], out_file);
    STRCPY(execvv[3], sol_file);
    execvv[4] = NULL;

    IFNEG_RET((pid_run = subprocess_run(sandbox, execvv, checker_bin,
                                        fd_in, fd_score[1], fd_err,
                                        true, exec_params,
                                        &fd_res)), ZEXE_CHKRERR);

    IFNEG_RET(z_close(fd_in),       ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_err),      ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_score[1]), ZEXE_SYSERR);


    // wait for all
    // ---------------
    // - runner    pid_run

    bool run_healthy = true;

    int st;
    while (true) {
        pid_t child = waitpid(-1, &st, 0);
        if (child < 0) {
            if (errno == ECHILD)
                break;
            return ZEXE_SYSERR;
        }

        __DEBUG("check (%s : %s, %s, %s) :: WAITING - "
                "GOT SIGNAL FROM %d: sig %d code %d ",
                sandbox, in_file, out_file, sol_file,
                child, WTERMSIG(st), WEXITSTATUS(st));

        if (child == pid_run) {
            // exited is okay! signaled is not!
            if (WIFSIGNALED(st) || (WIFEXITED(st) && WEXITSTATUS(st))) {
                run_healthy = false;
                __DEBUG("check (%s : %s, %s, %s) :: WAITING - "
                        "THAT WAS A TERMSIG OR EC !=0 FROM THE RUNNER!",
                        sandbox, in_file, out_file, sol_file);
                if (WIFSIGNALED(st) && (WTERMSIG(st) == SIGZERR)) {
                    __DEBUG("compile (%s : %s, %s, %s) :: THAT WAS A TERMSIG __SIGZERR__ "
                            "- LOOK AT zgrader_types.h CODE: EX__MAX + %d",
                            sandbox, in_file, out_file, sol_file,
                            (WEXITSTATUS(st) - EX__MAX));
                }
            }
        }

    }

    // if not healthy report system error, but close the file DESCRIPTOR_S_
    if (!run_healthy) {
        IFNEG_RET(z_close(fd_res),      ZEXE_SYSERR);
        IFNEG_RET(z_close(fd_score[0]), ZEXE_SYSERR);
        return ZEXE_SYSERR;
    }

    IFNEG_RET(read_exec_results(fd_res, exec_params, _exit_result), ZEXE_CHKRERR);

    LOGGER("check (%s : %s, %s, %s) :: signal : %d, exit : %d",
           sandbox, in_file, out_file, sol_file, _exit_result->exit_signal,
           _exit_result->exit_code);

    IFNEG_RET(read_score(fd_score[0], _pts), ZEXE_CHKRERR);

    LOGGER("check (%s : %s, %s, %s) :: SCORE: %d",
           sandbox, in_file, out_file, sol_file, *_pts);

    return 0;
}

/**
 * reads the score from the fiven fd
 *
 * @note: will close the fd after reading!
 */
int read_score(fd_t fd_score, int* _pts)
{
    FILE * f_score;
    IFEQ_RET((f_score = z_fdopen(fd_score, "r")), NULL, -1);

    int pts = 0;
    IFNEQ_RET(fscanf(f_score, "%d", &pts), 1, -1);

    IFNEQ_RET(z_fclose(f_score), (FILE*)0, -1);

    *_pts = pts;
    return 0;
}

int interactive(char *const exec_arv[],
                const char* sandbox,
                const char* exec_file,
                const char* checker_bin,
                const char* in_file,
                const char* sol_file,
                const char* eerr_file,
                const char* cerr_file,
                bool chrooted,
                const exec_params_st* exec_params,
                const exec_params_st* cexec_params,
                exec_results_st* _exit_result,
                exec_results_st* _cexit_result,
                int* _pts)
{

    LOGGER("interactive (%s : %s, %s, %s) :: limits:%d",
           sandbox, in_file, sol_file, checker_bin, exec_params->time_l);

    fd_t  fd_in, fd_sol;           // inputs to the bumper
    fd_t  fd_eerr, fd_cerr;        // error files
    fd_t  fd_ctoe[2], fd_etoc[2];  // pipes between the bumper and the program
    fd_t  fd_cres, fd_eres;        // execution results
    fd_t  fd_score[2];             // score pipe

    // open the files for the bumper
    IFNEG_RET(fd_in  = z_open2(in_file,  O_RDONLY), ZEXE_SYSERR);
    IFNEG_RET(fd_sol = z_open2(sol_file, O_RDONLY), ZEXE_SYSERR);

    // open the pipes between the two programs
    IFNEG_RET(z_pipe(fd_ctoe), ZEXE_SYSERR);
    IFNEG_RET(z_pipe(fd_etoc), ZEXE_SYSERR);

    // score pipe
    IFNEG_RET(z_pipe(fd_score), ZEXE_SYSERR);

    pid_t pid_erun, pid_crun;

    // get the error collectors
    IFNEG_RET(fd_eerr = get_collect(eerr_file), ZEXE_SYSERR);
    IFNEG_RET(fd_cerr = get_collect(cerr_file), ZEXE_SYSERR);

    fd_t  fd_stdin, fd_stdout;
    // kill the stdin/stdout for the checker
    IFNEG_RET(fd_stdin   = z_open2("/dev/null", O_RDONLY), ZEXE_SYSERR);
    IFNEG_RET(fd_stdout  = z_open2("/dev/null", O_WRONLY), ZEXE_SYSERR);

    // generate the arguments for the checker execution
    char *execvv[7];
    STRCPY(execvv[0], checker_bin);
    STRPRT(execvv[1], "%d", fd_etoc[0]);  // data from the user
    STRPRT(execvv[2], "%d", fd_ctoe[1]);  // data to the user
    STRPRT(execvv[3], "%d", fd_in);       // input file
    STRPRT(execvv[4], "%d", fd_sol);      // sol file
    STRPRT(execvv[5], "%d", fd_score[1]); // score pipe
    execvv[6] = NULL;

    // EXECUTE BOTH!
    IFNEG_RET((pid_crun = subprocess_run(sandbox, execvv, checker_bin,
                                         fd_stdin, fd_stdout, fd_cerr,
                                         true, cexec_params,
                                         &fd_cres)), ZEXE_CHKRERR);

    IFNEG_RET((pid_erun = subprocess_run(sandbox, exec_arv, exec_file,
                                         fd_ctoe[0], fd_etoc[1], fd_eerr,
                                         chrooted, exec_params,
                                         &fd_eres)), ZEXE_SYSERR);


    IFNEG_RET(z_close(fd_stdin),    ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_stdout),   ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_in),       ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_sol),      ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_cerr),     ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_eerr),     ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_ctoe[0]),  ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_ctoe[1]),  ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_etoc[0]),  ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_etoc[1]),  ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_score[1]), ZEXE_SYSERR);

    // wait for all
    // ---------------
    // - runner    pid_crun
    // - runner    pid_erun

    bool run_chealthy = true, run_ehealthy = true;

    int st;
    while (true) {
        pid_t child = waitpid(pid_erun, &st, 0);
        if (child < 0) {
            run_ehealthy = run_ehealthy && (errno == ECHILD);
            break;
        }

        __DEBUG("interactive (%s : %s, %s, %s) :: WAITING - "
                "GOT SIGNAL FROM %d: sig %d code %d ",
                sandbox, in_file, sol_file, checker_bin,
                child, WTERMSIG(st), WEXITSTATUS(st));

        if (child == pid_erun) {
            // exited is okay! signaled is not!
            if (WIFSIGNALED(st) || (WIFEXITED(st) && WEXITSTATUS(st))) {
                run_ehealthy = false;
                __DEBUG("interactive (%s : %s, %s, %s) :: WAITING - "
                        "THAT WAS A TERMSIG OR EC !=0 FROM THE RUNNER!",
                        sandbox, in_file, sol_file, checker_bin);
                if (WIFSIGNALED(st) && (WTERMSIG(st) == SIGZERR)) {
                    __DEBUG("interactive (%s : %s, %s, %s) :: "
                            "THAT WAS A TERMSIG __SIGZERR__ "
                            "- LOOK AT zgrader_types.h CODE: EX__MAX + %d",
                            sandbox, in_file, sol_file, checker_bin,
                            (WEXITSTATUS(st) - EX__MAX));
                }
            }
        }

    }

    if (!run_ehealthy) {
        kill(pid_crun, SIGKILL);
        run_chealthy = false;
    } else {
        // get the exit results
        IFNEG_RET(read_exec_results(fd_eres,
                                    exec_params,
                                    _exit_result),  ZEXE_SYSERR);

        if (_exit_result->result != 0) {
            kill(pid_crun, SIGKILL);
            run_ehealthy = run_chealthy = false;
        }
    }

    while (true) {
        pid_t child = waitpid(pid_crun, &st, 0);
        if (child < 0) {
            run_chealthy = run_chealthy && (errno == ECHILD);
            break;
        }

        if (child == pid_crun) {
            // exited is okay! signaled is not!
            if (WIFSIGNALED(st) || (WIFEXITED(st) && WEXITSTATUS(st))) {
                run_chealthy = false;
                __DEBUG("interactive (%s : %s, %s, %s) :: WAITING - "
                        "THAT WAS A TERMSIG OR EC !=0 FROM THE RUNNER!",
                        sandbox, in_file, sol_file, checker_bin);
                if (WIFSIGNALED(st) && (WTERMSIG(st) == SIGZERR)) {
                    __DEBUG("interactive (%s : %s, %s, %s) :: "
                            "THAT WAS A TERMSIG __SIGZERR__ "
                            "- LOOK AT zgrader_types.h CODE: EX__MAX + %d",
                            sandbox, in_file, sol_file, checker_bin,
                            (WEXITSTATUS(st) - EX__MAX));
                }
            }
        }
    }

    // something has failed
    if (!(run_ehealthy && run_chealthy)) {
        IFNEG_RET(z_close(fd_score[0]), ZEXE_SYSERR);
        IFNEG_RET(z_close(fd_eres),     ZEXE_SYSERR);
        IFNEG_RET(z_close(fd_cres),     ZEXE_SYSERR);
        return ZEXE_SYSERR;
    }

    IFNEG_RET(read_exec_results(fd_cres,
                                cexec_params,
                                _cexit_result), ZEXE_CHKRERR);

    // read the score
    if ((_exit_result->result == 0) && (_cexit_result->result == 0)) {
        IFNEG_RET(read_score(fd_score[0], _pts), ZEXE_CHKRERR);
    } else {
        *_pts = 0;
    }

    return 0;

}
