/*
 * Copyright (c) 2009-2013 z-training -- http://z-trening.com
 * AUTHORS:
 *   Aleksandar Zlateski (aleksandar.zlateski@gmail.com)
 *
 */

#ifndef ZI_ZGRADER_LANGUAGES_H_
#define ZI_ZGRADER_LANGUAGES_H_

#include "zgrader_types.h"
#include "zgrader_macros.h"

/* prog langs */
#define ZLANG_FPC             0x00010001
#define ZLANG_C               0x00010002
#define ZLANG_CPP             0x00010003
#define ZLANG_JAVA            0x01010004
#define ZLANG_PYTHON          0x01000005
#define ZLANG_PHP             0x01000006
#define ZLANG_PERL            0x01000007
#define ZLANG_RUBY            0x01000008
#define ZLANG_SCHEME          0x01000009
#define ZLANG_FORTRAN         0x0001000a
#define ZLANG_YBASIC          0x0100000b
#define ZLANG_MATLAB          0x0100000c
#define ZLANG_GPC             0x0001000d
#define ZLANG_CPP11           0x0001000e
#define ZLANG_CS              0x0101000f

// TODO: do it better!
#define JAVA_CLASS_PATH       "/lib/zjava.zip"

#define ZLANGCOMPILES(x)      (x & 0x0001000)
#define ZLANGINTERPRETS(x)    (x & 0x0100000)

#define lang_t int

bool get_run_command(      lang_t lang,
                           int time_l,
                           int mem_l,
                           const char* compiled,
                           char *_command[],
                           bool *_chroot_l,
                           int *_time_l,
                           int *_mem_l,
                           bool *_proc_l);

bool get_compile_command(  lang_t lang,
                           int time_l,
                           const char* source,
                           const char* executable,
                           char *_command[],
                           int *_time_l);

bool get_source_bin_names( lang_t lang,
                           char* source,
                           char* executable);

#endif
