/*
 * Copyright (c) 2009-2012 z-training -- http://z-training.net
 * AUTHORS:
 *
 *   Aleksandar Zlateski (admin@z-trening.com, z@zlateski.com)
 *
 */

#include "zgrader.h"

/**
 *                      +----------+
 *                      |    US    |
 *                      +----------+
 *                          | fork()
 *                      +----------+
 *                      | pid_run  |
 *                      +----------+
 *                          | fork()+exec()
 *                      +----------+
 *   IN_FILE ---------->| PROGRAM  |--------> OUT_FILE
 *             stdin    +----------+ stdout
 *                           |
 *                           +---------> ERR_FILE
 *                             stderr
 *
 * @return int ZEXE_SYSERR  system error, something wrong with the grader
 *             ZEXE_TLE     time limit exceeded
 *             ZEXE_RTE     run time error
 *             ZEXE_OLE     optut limit exceeded
 *             ZEXE_MLE     memory limit exceeded
 *             ZEXE_NOT0    return code not 0
 */
int execute(char* const exec_arv[],
            const char* sandbox,
            const char* exec_file,
            const char* in_file,
            const char* out_file,
            const char* err_file,
            bool chrooted,
            const exec_params_st* exec_params,
            exec_results_st* _exit_result)
{

    // ANY ERROR HERE IS A SYSTEM ERROR (NOT USER'S PROGRAM ERROR)
    // HOWERVER, IT MIGHT BE (AND USUALLY IS) CAUSED BY THE USER'S PROGRAM

    LOGGER("execute (%s : %s) :: START EXECUTION WITH LIMITS: %d:%d:%d",
           sandbox, exec_file, exec_params->time_l,
           exec_params->mem_l, exec_params->out_l);

    fd_t  fd_in, fd_out, fd_err, fd_res;
    pid_t pid_run;

    IFNEG_RET(fd_in  = get_feeder(in_file),   ZEXE_SYSERR);
    IFNEG_RET(fd_out = get_collect(out_file), ZEXE_SYSERR);
    IFNEG_RET(fd_err = get_collect(err_file), ZEXE_SYSERR);

    IFNEG_RET((pid_run = subprocess_run(sandbox, exec_arv, exec_file,
                                        fd_in, fd_out, fd_err,
                                        chrooted, exec_params,
                                        &fd_res)), ZEXE_SYSERR);

    IFNEG_RET(z_close(fd_in),  ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_out), ZEXE_SYSERR);
    IFNEG_RET(z_close(fd_err), ZEXE_SYSERR);

    // wait for all
    // ---------------
    // we have to wait for:
    // - runner    pid_run (process running the user's file)
    //
    // we don't really monitor which children die here. we just want to
    // make sure there are no more children before we decide to stop waiting
    // we don't want any zombies around.
    //
    // we also monitor whether the pid_run had been killed, since in that
    // case we are not going to be able to read the results from the fd_res
    bool run_healthy = true;

    int st;
    while (true) {
        pid_t child = waitpid(-1, &st, 0);
        if (child < 0)
        {
            if (errno == ECHILD)
                break;
            return ZEXE_SYSERR;
        }

        __DEBUG("execute (%s : %s) :: WAITING - "
                "GOT SIGNAL FROM %d: sig %d code %d ",
                sandbox, exec_file, child, WTERMSIG(st), WEXITSTATUS(st));

        if (child == pid_run)
        {
            // exited is okay! signaled is not!
            // we made sure that we exit with a signal code (SIGZERR)
            // however we can also check the exit code, just in case
            if (WIFSIGNALED(st) || (WIFEXITED(st) && WEXITSTATUS(st))) {
                run_healthy = false;
                __DEBUG("execute (%s : %s) :: WAITING - "
                        "THAT WAS A TERMSIG OR EC !=0 FROM THE RUNNER!",
                        sandbox, exec_file);
                if (WIFSIGNALED(st) && (WTERMSIG(st) == SIGZERR)) {
                    __DEBUG("execute (%s : %s) :: THAT WAS A TERMSIG __SIGZERR__ - "
                            "LOOK AT zgrader_types.h CODE: EX__MAX + %d",
                            sandbox, exec_file, (WEXITSTATUS(st) - EX__MAX));
                }
            }
        }

    }

    // if not healthy report system error, but close the file descriptor
    if (!run_healthy) {
        IFNEG_RET(z_close(fd_res), ZEXE_SYSERR);
        return ZEXE_SYSERR;
    }

    // get result status
    IFNEG_RET(read_exec_results(fd_res,
                                exec_params,
                                _exit_result), ZEXE_SYSERR);

    LOGGER("execute (%s : %s) :: signal : %d, exit : %d",
           sandbox, exec_file, _exit_result->exit_signal,
           _exit_result->exit_code);

    return _exit_result->result;
}

/**
 * Executes the exec_file chrooted in sandbox_path
 *
 * @param  sandbox     absolute path to the execution sandbox
 * @param  exec_arv    NULL terminated array of execution arguments with
 *                     absolute paths (can be ./ if the execution is chrooted)
 * @param  exec_file   the file to be executed (binary) or interpreted
 *                     (.class for java, .php for php... etc...)
 * @param  fd_in       the file descriptor number for the standard input
 * @param  fd_out      the file descriptor number for the standard output
 * @param  fd_err      the file descriptor number for the standard error
 * @param  chrted      whether to chroot to the sandbox or not
 * @param  exec_params execution parameters (time, memory, output limits,
 *                     uid and gid for the exececution)
 * @param[O] _res_pipe result's pipe input descriptor
 *
 * @return ZSBXFCLOSE  failed to close the result pipe's input
 *         ZSBXFPIPE   failed to create the result pipe
 *         ZSBXFFORK   failed to fork
 *         pid_t       pid of the runner process
 */
pid_t subprocess_run(const char* sandbox,
                     char *const exec_arv[],
                     const char* exec_file,
                     fd_t fd_in,
                     fd_t fd_out,
                     fd_t fd_err,
                     bool chrted,
                     const exec_params_st* exec_params,
                     fd_t *_res_pipe)
{

    // forked runner ....
    // ------------------------
    // we get the results through a pipe. we will return the pid
    // of the processes doing the actual execution. we have to
    // make sure that this wrapper process NEVER failes, and always
    // writes the results to the pipe.
    //
    // we only allow (as for now) this process to be killed by
    // a system error, which will result in another system error, but
    // we don't really care a we want to clean up the processess

    fd_t res_pipe[2];
    IFNEG_RET(z_pipe(res_pipe), ZSBXFPIPE);

    pid_t pid;
    int keep[] = {fd_in, fd_out, fd_err, res_pipe[1]};
    IFNEG_RET((pid = z_fork(keep, 4)), ZSBXFFORK);

    // running child
    // ------------------
    // @exit SIGZERR, ZSBXFFDOPEN  couldn't open the pipe for writing results
    //       SIGZERR, ZSBXFFCLOSE  couldn't close the pipe for wrtng results
    //       exec_arv's EXITSIG, EXITCODE otherwise
    if (pid == 0) {

        __DEBUG("subprocess_run_child (%s : %s) :: CHILD STARTED",
                sandbox, exec_file);
        double time;
        int memory, status, result;

        result = run_sandboxed(sandbox, exec_arv, exec_file,
                               fd_in, fd_out, fd_err,
                               chrted, exec_params,
                               &time, &memory, &status);

        __DEBUG("subprocess_run_child (%s : %s) :: GOT THE RESULTS, OPENING THE FD",
                sandbox, exec_file);

        FILE *f_res;
        IFEQ_EXIT((f_res = z_fdopen(res_pipe[1], "w")), NULL, ZSBXFFDOPEN);

        __DEBUG("subprocess_run_child (%s : %s) :: PIPING RESULT - "
                "res: %d; tme: %lf ms; %d KB; stat: %d",
                sandbox, exec_file, result, time, memory, status);

        // this doesn't go around an error. we don't really get anything if
        // we catch an error here
        fprintf(f_res, "%d ; %lf ; %d ; %d", result, time, memory, status);
        // we'll catch it right here :)
        IFNEQ_EXIT(z_fclose(f_res), 0, ZSBXFFCLOSE);

        LOGGER("subprocess_run_child (%s : %s) :: RUNNER SUBPROCESS ALL DONE!",
               sandbox, exec_file);
        exit(0);
    }

    LOGGER("subprocess_run (%s : %s) :: RUNNER RUNNING WITH PID %d",
           sandbox, exec_file, pid);

    IFNEG_RET(z_close(res_pipe[1]), ZSBXFCLOSE);

    // you can read the results from here
    *_res_pipe = res_pipe[0];
    return pid;
}

/*
 *
 */
int run_sandboxed(const char* sandbox,
                  char* const exec_arv[],
                  const char* exec_file,
                  fd_t fd_in,
                  fd_t fd_out,
                  fd_t fd_err,
                  bool chrted,
                  const exec_params_st* exec_params,
                  double* _time,
                  int* _memory,
                  int* _status)
{

    pid_t pid;

    int keep[] = {fd_in, fd_out, fd_err};
    IFNEG_RET((pid = z_fork(keep, 3)), ZSBXFFORK);

    if (pid == 0) {

        // stdin, stdout and stderr
        __DEBUG("run_sandboxed_child (%s : %s) :: FORKED", sandbox, exec_file);

        IFNEG_EXIT(close(STDIN_FILENO),  __ZSIG(ZSBXFCLOSE));
        IFNEG_EXIT(close(STDOUT_FILENO), __ZSIG(ZSBXFCLOSE));
        IFNEG_EXIT(close(STDERR_FILENO), __ZSIG(ZSBXFCLOSE));

        IFNEQ_EXIT(dup2(fd_in,  STDIN_FILENO),  STDIN_FILENO,  __ZSIG(ZSBXFDUP2));
        IFNEQ_EXIT(dup2(fd_out, STDOUT_FILENO), STDOUT_FILENO, __ZSIG(ZSBXFDUP2));
        IFNEQ_EXIT(dup2(fd_err, STDERR_FILENO), STDERR_FILENO, __ZSIG(ZSBXFDUP2));

        __DEBUG("run_sandboxed_child (%s : %s) :: SET THE FILES, SETTING THE "
                "LIMITS: ", sandbox, exec_file);

        IFNEG_EXIT(chdir(sandbox),                           __ZSIG(ZSBXFCHDIR));

        __DEBUG("run_sandboxed_child (%s : %s) :: TRYING TO OWN %s AS %d: %d ",
                sandbox, exec_file, exec_file, exec_params->uid, exec_params->gid);
        IFNEG_EXIT(chown(exec_file, exec_params->uid,
                         exec_params->gid),                  __ZSIG(ZSBXFCHOWN));
        IFNEG_EXIT(chmod(exec_file, S_IRWXU | S_IXGRP | S_IXOTH),
                   __ZSIG(ZSBXFCHMOD));

        __DEBUG("run_sandboxed_child (%s : %s) :: CHDIRED, CHOWNED AND CHMODED",
                sandbox, exec_file);

        if (chrted) {
            IFNEG_EXIT(chroot(sandbox),                __ZSIG(ZSBXFCHROOT));
            __DEBUG("run_sandboxed_child (%s : %s) :: CHROOTED", sandbox, exec_file);
        }

        // set the execution limits and uid/gid
        int r;
        IFNEG_EXIT(r = apply_exec_params(exec_params), __ZSIG(r));

        __DEBUG("run_sandboxed_child (%s : %s) :: SET THE LIMTS UID: %d, GID: %d",
                sandbox, exec_file, exec_params->uid, exec_params->gid);

        LOGGER("run_sandboxed_child (%s : %s) :: EXECUTING (execv)",
               sandbox, exec_file);


        // @TODO: make loggerv to log this
        // PRINT_EXECV(logger_f, exec_arv);

        IFNEG_EXIT(ptrace(PTRACE_TRACEME, 0, NULL, NULL), __ZSIG(ZSBXFPTRACEME));

        __DEBUG("run_sandboxed_child (%s : %s) :: RUNNING BINARY (%s)",
                sandbox, exec_file, exec_arv[0]);

        IFNEG_EXIT(execv(exec_arv[0], exec_arv),          __ZSIG(ZSBXFEXEC));

        exit(__ZSIG(ZSBXUNKNOWN));

    }

    LOGGER("run_sandboxed (%s : %s) :: PROGRAM RUNNING WITH PID %d",
           sandbox, exec_file, pid);

    //IFNEG_RET(z_close(fd_in),  ZSBXFCLOSE);
    //IFNEG_RET(z_close(fd_out), ZSBXFCLOSE);
    //IFNEG_RET(z_close(fd_err), ZSBXFCLOSE);

    int status;
    struct rusage rusage;

    IFNEG_RET(wait4(pid, &status, 0, &rusage), ZSBXFWAIT4);

    // this will fix the problem with MLE => TLE !!!!
    // ----------------------------------------------
    // it looks like setting rlimit for the address space
    // would not let an elf program load at all, the process
    // get's killed before the elf is loaded with SIGKILL
    // here we can catch that and modify the exit signal to
    // SIGSEGV (MLE)
    // @note: look at <bits/signum.h>

    if (WIFSIGNALED(status)) {
        __DEBUG("run_sandboxed (%s : %s) :: Early exit! Probably MLE "
                "(stat: %d sig: %d code: %d)",
                sandbox, exec_file, status, WTERMSIG(status), WEXITSTATUS(status));
        *_status = __W_EXITCODE(WEXITSTATUS(status), SIGSEGV);
        return 0;
    }

    // initial PTRACE call !!!!
    // -----------------------
    // we expect, and we MUST get a stopped child. If the child
    // managed to escape somehow, we kill it. If any other error
    // occured, we still kill the child, and we'll ultimately report
    // execution error - other errors (TLE,...) could not have happened
    // at this point, and we've taken care of MLE
    IF_RET(!WIFSTOPPED(status), ZSBXRNOTPTRACED);

    // got back from ptrace
    LOGGER("run_sandboxed (%s : %s) :: GOT BACK FROM PTRACE, READY TO MEASURE",
           sandbox, exec_file);

    // we take this moment as an official start of the program's execution
    // at this point the elf has loaded and is ready to run
    double zero_usage;
    zero_usage = (rusage.ru_utime.tv_sec + rusage.ru_stime.tv_sec) * 1000
        + (rusage.ru_utime.tv_usec + rusage.ru_stime.tv_usec) / 1000;

    // this should be very close to zero (since we don't allow huge programs)
    // but in the case of heavy interpretators, it can take a while
    __DEBUG("run_sandboxed (%s : %s) :: ZERO TIME = %5.2f",
            sandbox, exec_file, zero_usage);

    // we want the child to trace child's exit
    IFNEG_RET(ptrace(PTRACE_SETOPTIONS, pid, 0, PTRACE_O_TRACEEXIT), -6);
    IFNEG_RET(ptrace(PTRACE_CONT, pid, NULL, NULL), -6);

    // setup sig alarm
    // ---------------
    // even though the rlimit will kill SIGTERM the child, we want
    // to make sure that the child dosn't just sleep, and not use any
    // resource. we assume the program uses the resource as much as possible
    // and the REALTIME ~= CPUTIME. we still allow 1.5x time
    run_pid = pid;
    IFEQ_RET(signal(SIGALRM, alarm_handler), SIG_ERR, ZSBXFSSGNL);
    // @todo:: figure out better alarm strategy then just 1.5x !
    ITIMERVALSET((3*exec_params->time_l) / 2,         ZSBXFSITIMER);

    // running with PTRACE strategy:
    // -----------------------------
    // we get the child's status. if it is an actual real exit. we can continue
    // otherwise we can have the following cases:
    // SIGNALED - child was sent a signal (resource limits, time limits, etc...)
    // EXITED   - this should actually never happen, but we'll treat it ~SIGNALED
    // STOPPED(for any other reason) - might happen with the interpreters, that
    // fork, etc... we just send the CONT signal and iterate

    while (true) { // running the program with ptrace!

        __DEBUG("run_sandboxed (%s : %s) :: PTRACE ITERATION!", sandbox, exec_file);

        IFNEG_RET(wait4(pid, &status, 0, &rusage), ZSBXFWAIT4);

        if (WIFSTOPPED(status) && (WSTOPSIG(status) == SIGTRAP) &&
            (status & (PTRACE_EVENT_EXIT << 8))) {
            __DEBUG("run_sandboxed (%s : %s) :: GOT A REAL __EXIT__ SIGNAL",
                    sandbox, exec_file);
            break;
        }

        if (WIFEXITED(status) || WIFSIGNALED(status)) {
            __DEBUG("run_sandboxed (%s : %s) :: SIGNALED (OR EXITED), "
                    "REPORTING THE SIGNAL (%d: %d, %d)",
                    sandbox, exec_file, WTERMSIG(status), WEXITSTATUS(status));
            *_status = status;
            return ZSBXREARLYEXIT;
        }

        __DEBUG("run_sandboxed (%s : %s) :: PROBABLY STOPPED ? %d WITH %d",
                sandbox, exec_file, WIFSTOPPED(status), WSTOPSIG(status));

        // we'll try CONT ANYWAYS, failure will utimately result in EXEC ERROR
        IFNEG_RET(ptrace(PTRACE_CONT, pid, 0, WSTOPSIG(status)), ZSBXFPTCECONT);
    }

    // get usages!
    *_time = (rusage.ru_utime.tv_sec + rusage.ru_stime.tv_sec) * 1000
        + (rusage.ru_utime.tv_usec + rusage.ru_stime.tv_usec) / 1000
        - zero_usage;

    //IFNEG_RET((*_memory = get_mem_usage(pid)), ZSBXFGMEMUSG);
    *_memory = rusage.ru_maxrss;

    __DEBUG("run_sandboxed (%s : %s) :: GOT USAGES: TIME: %5.2f ms, MEM: %d KB, RSS: %d",
            sandbox, exec_file, *_time, *_memory, rusage.ru_maxrss);

    // get the real status
    IFNEG_RET(ptrace(PTRACE_GETEVENTMSG, pid, 0, &status), ZSBXFPTCEGENV);
    *_status = status;

    __DEBUG("run_sandboxed (%s : %s) :: GOT THE EXIT STAT (%d : %d, %d : %d, %d)",
            sandbox, exec_file, status, WIFSTOPPED(status), WSTOPSIG(status),
            WTERMSIG(status), WEXITSTATUS(status));

    // let the child die
    // -----------------
    // after the actual exit call, we will get one more
    // SIGTRAP from the child - not sure why, but that's the way it is
    // in LINUIX - POSIX and BSD do not do this, so we safely
    // handle that case

    errno = 0;
    (ptrace(PTRACE_CONT, pid, 0, 0),  ZSBXFPTCECONT);
    if ( errno == ESRCH )
    {
        __DEBUG("errno == ESRCH");
    }
    else
    {
        while (true) {
            IFNEG_RET(wait4(pid, &status, 0, &rusage), ZSBXFWAIT4);
            if (WIFSTOPPED(status) && (WSTOPSIG(status) == SIGTRAP)) {
                // this is extra PTRACE CALL
                // extra PTRACE probably means extra children
                // therefore, get the usages again!
                __DEBUG("run_sandboxed (%s : %s) :: EXTRA SIGTRAP (%d : STOPSIG: %d)",
                        sandbox, exec_file, status, WSTOPSIG(status));
                IFNEG_RET(ptrace(PTRACE_CONT, pid, 0, 0),  ZSBXFPTCECONT);

                // get usages again!
                *_time = (rusage.ru_utime.tv_sec + rusage.ru_stime.tv_sec) * 1000
                    + (rusage.ru_utime.tv_usec + rusage.ru_stime.tv_usec) / 1000
                    - zero_usage;
            } else {
                IF_RET(!(WIFEXITED(status) || WIFSIGNALED(status)), ZSBXUNKNOWN);
                __DEBUG("run_sandboxed (%s : %s) :: WE ARE DONE! EXITED %d OR SIGNLED %d",
                        sandbox, exec_file, WIFEXITED(status), WIFSIGNALED(status));
                *_status = status;
                break;
            }
        }
    }

    // no need for the alarm anymore
    IFEQ_RET(signal(SIGALRM, SIG_IGN), SIG_ERR, ZSBXFSSGNL);

    LOGGER("run_sandboxed (%s : %s) :: time: "
           "%2.5f ms, mem %d KB, sig: %d, exitst: %d",
           sandbox, exec_file, *_time, *_memory,
           WTERMSIG(status), WEXITSTATUS(status));

    return 0;
}

/**
 * translates exit status to ZEXE return code
 *
 */
int get_exec_result(int status,
                    const exec_params_st* exec_params,
                    const exec_results_st* exec_result)
{

    int exit_signal = WTERMSIG(status);
    int exit_code   = WEXITSTATUS(status);

    if (exit_code != 0)
        return ZEXE_NOT0;

    switch (exit_signal)  {
    case 0:
        break;
    case SIGXCPU:
    case SIGKILL:
        return ZEXE_TLE;
    case SIGSEGV:
        // LINUX throws SIGIOT on malloc when the rlimit is exceeded
        // unlike BSD that throws SIGSEGV always. SIGSEGV is called
        // when the limit is exceeded during stack allocation.
        // we hope (and are pretty sure) no other error throws SIGIOT
        // @note: look at <bits/signum.h>
    case SIGIOT:
        return ZEXE_MLE;
    case SIGXFSZ:
        return ZEXE_OLE;
    default:
        return ZEXE_RTE;
    }

    // make sure we are not over the limits
    if (exec_params->time_l < exec_result->time)
        return ZEXE_TLE;

    // exec_result->memory is in KB, exec_params->mem_l is in Bytes
    if (exec_params->mem_l && (exec_params->mem_l < exec_result->memory << 10))
        return ZEXE_MLE;

    return 0;
}

/*
 * gets the result status from the file descriptor fd_res
 *
 * @note: will close the fd after reading!
 */
int read_exec_results(fd_t fd_res,
                      const exec_params_st* exec_params,
                      exec_results_st* _exec_result)
{

    FILE * f_res;
    IFEQ_RET((f_res = z_fdopen(fd_res, "r")), NULL, -1);

    int status;
    IFNEQ_RET(fscanf(f_res, " %d ; %lf ; %d ; %d",
                     &_exec_result->runner_result,
                     &_exec_result->time,
                     &_exec_result->memory,
                     &status), 4, -1);

    IFNEQ_RET(z_fclose(f_res), (FILE*)0, -1);

    _exec_result->exit_signal = WTERMSIG(status);
    _exec_result->exit_code   = WEXITSTATUS(status);
    _exec_result->status      = status;

    if (_exec_result->runner_result != 0) {
        _exec_result->result = ZEXE_SYSERR;
    } else {
        _exec_result->result = get_exec_result(status,
                                               exec_params,
                                               _exec_result);
    }

    __DEBUG(">> read_exec_results() :: res: %d, "
            "exit_signal: %d, exit_code: %d",
            _exec_result->result, _exec_result->exit_signal,
            _exec_result->exit_code);

    return 0;
}

/**
 * applies the given params (limits)
 *
 * @return 0 on success, error number otherwise
 */
int apply_exec_params(const exec_params_st* exec_params)
{

    rlimit_st rlim;

    if (exec_params->mem_l > 0) {
        rlim.rlim_cur = rlim.rlim_max = (10 << 20) + (exec_params->mem_l / 4) * 5 ;
        IFNEG_RET(setrlimit(RLIMIT_AS, &rlim), ZSBXFRLIMIT);
        __DEBUG(">> apply_exec_params :: SETTING MEML TO %d", rlim.rlim_max);
    }

    if (exec_params->nproc_l > 0) {
        rlim.rlim_cur = rlim.rlim_max = exec_params->nproc_l;
        IFNEG_RET(setrlimit(RLIMIT_NPROC, &rlim), ZSBXFRLIMIT);
        __DEBUG(">> apply_exec_params :: SETTING PRCL TO %d", rlim.rlim_max);
    }

    if (exec_params->time_l > 0) {
        rlim.rlim_cur = rlim.rlim_max = (exec_params->time_l / 1000) + 1;
        IFNEG_RET(setrlimit(RLIMIT_CPU, &rlim), ZSBXFRLIMIT);
        __DEBUG(">> apply_exec_params :: SETTING TMEL TO %d", rlim.rlim_max);
    }

    if (exec_params->out_l > 0) {
        rlim.rlim_cur = rlim.rlim_max = exec_params->out_l;
        IFNEG_RET(setrlimit(RLIMIT_FSIZE, &rlim), ZSBXFRLIMIT);
        __DEBUG(">> apply_exec_params :: SETTING OUTL TO %d", rlim.rlim_max);
    }

    __DEBUG(">> apply_exec_params :: SETTING UID/GID %d, %d",
            exec_params->uid, exec_params->gid);
    if (exec_params->gid) IFNEG_RET(setgid(exec_params->gid), ZSBXFSGID);
    if (exec_params->uid) IFNEG_RET(setuid(exec_params->uid), ZSBXFSUID);

    return 0;
}
