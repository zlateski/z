/*
 * Copyright (c) 2009-2010 z-training -- http://z-training.net
 *
 * AUTHORS:
 *   Aleksandar Zlateski (admin@z-trening.com, z@zlateski.com)
 *
 */

#include <dirent.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include "zgrader.h"
#include "zgrader_macros.h"

#ifdef DEBUG_MODE
char     log_spaces[10];
int      log_offset;
#endif

FILE*    logger_f;
char*    log_fname;
// logging semaphore key
int fmutex;

void logger(const char *format, ...)
{

    time_t t;
    time(&t);
    struct tm* time;
    time = localtime(&t);

    fprintf(logger_f, "[%04d/%02d/%02d %02d:%02d:%02d]  ::  ",
            time->tm_year+1900,
            time->tm_mon+1, time->tm_mday, time->tm_hour,
            time->tm_min, time->tm_sec);

#ifdef DEBUG_MODE
    fprintf(logger_f, "\t%s", log_spaces);
#endif

    va_list args;
    va_start(args, format);

    vfprintf(logger_f, format, args);
    fprintf(logger_f, "\n");
    fflush(logger_f);
}

int post_log()
{

    //SEM_ACQUIRE();
    fd_t fd = open(log_fname, O_WRONLY | O_CREAT | O_APPEND,
                   S_IRWXU | S_IRGRP | S_IROTH);
    FILE* fout = fdopen(fd, "a");

    char tmp_log_file[1024];
    sprintf(tmp_log_file, "/tmp/%d.log", getpid());
    FILE* fin  = fopen(tmp_log_file, "r");

    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    while ((read = getline(&line, &len, fin)) != -1) {
        fprintf(fout, "%s | %s", "GRADER_LOG", line);
    }
    if (line)
        free(line);

    fclose(fout);
    fclose(fin);
    //SEM_RELEASE();

    unlink(tmp_log_file);

    return 0;
}

int init_grader(const char *log_file)
{
#ifdef DEBUG_MODE
    log_offset = 0;
    log_spaces[0] = '\0';
#endif

    STRCPY(log_fname, log_file);

    key_t skey = FTOK(1);
    // @todo: it is fatal error, for now we exit, but we can
    //        design this better
    IFNEG_RET(fmutex = semget(skey, 1, IPC_CREAT), -1);

    run_pid = 0;
    LIST_INIT(&fd_list);
    LIST_INIT(&pid_list);

    char tmp_log_file[1024];
    sprintf(tmp_log_file, "/tmp/%d.log", getpid());

    int logger_fd = open(tmp_log_file, O_WRONLY | O_CREAT | O_TRUNC,
                         S_IRWXU | S_IRGRP | S_IROTH);
    logger_f = fdopen(logger_fd, "w");

    return 0;
}

/**
 * will get the (maximal) memory usage for the given pid.
 * the usage is obtained from the proc filesystem, hence
 * we need the process to be alive during the execution of
 * this function. That's why all our executions are ptraced!
 *
 * @return int  maximal memory usage in KB
 *          -1  on error
 */
int get_mem_usage(pid_t pid)
{
    char file[1024];
    char buffer[4001];

    sprintf(file, "/proc/%d/status", pid);

    fd_t fd_in;
    IFNEG_RET((fd_in = z_open2(file, O_RDONLY)), -1);

    int n;
    IFNEG_RET((n = read(fd_in, buffer, 4000)), -1);
    IFNEG_RET(z_close(fd_in),                  -1);

    buffer[n] = '\0';

    char *hit;
    IF_RET(!(hit = strstr(buffer, "VmPeak")), -1);

    long vmpeak, vmsize, vmlck, vmhwm, vmrss, vmdata, vmstk, vmexe, vmlib, vmpte;
    sscanf(hit, "VmPeak: %ld kB\nVmSize: %ld kB\n"
           "VmLck: %ld kB\nVmHWM: %ld kB\nVmRSS: %ld kB\nVmData: %ld kB\n"
           "VmStk: %ld kB\nVmExe: %ld kB\nVmLib: %ld kB\nVmPTE: %ld kB\n",
           &vmpeak, &vmsize, &vmlck, &vmhwm,&vmrss, &vmdata, &vmstk, &vmexe,
           &vmlib, &vmpte);

    __DEBUG("__ get_mem_usage (%d) :: read: %ld %ld %ld",
            pid, vmpeak, vmexe, vmpte);
    return MAX(0, (vmpeak - vmexe - vmpte));
}

/*
 * will open the file for read only access and return the
 * file descriptor of the file
 *
 * @param  file_in  the absolute path to the input file_in
 *
 * @return fd_t     file descriptor of the input
 *         -1       on error
 *
 */
fd_t get_feeder(const char* file_in)
{
    stat_st fstat;
    fd_t fd;

    // check the input file
    IFNEG_RET(stat(file_in, &fstat), -1);

    IFNEG_RET((fd = z_open2(file_in, O_RDONLY)), -1);
    return fd;
}

/*
 *
 */
fd_t get_collect(const char* file_out)
{
    fd_t fd;
    IFNEG_RET(fd = z_open3(file_out, O_WRONLY | O_CREAT | O_TRUNC,
                           S_IRWXU | S_IRGRP | S_IROTH), -1);
    return fd;
}

/*
 *
 */
int remove_dir(const char* dir_path)
{

    stat_st statbuf;

    if (lstat(dir_path, &statbuf) < 0) {
        // doesn't exist - we are all set!
        return 0;
    }

    if (S_ISREG(statbuf.st_mode) || S_ISLNK(statbuf.st_mode)) {
        // file or link - delete it
        return remove(dir_path);
    }

    if (!S_ISDIR(statbuf.st_mode)) {
        // anything else -- error!
        return -1;
    }

    DIR* dp = opendir(dir_path);
    if (dp == NULL)
        return -1;

    struct dirent *dirp;
    char buf[1024];

    while ( (dirp = readdir(dp) ) != NULL ) {
        if ( (strcmp(dirp->d_name, ".") != 0) &&
             (strcmp(dirp->d_name, "..") != 0) ) {

            sprintf(buf, "%s/%s", dir_path, dirp->d_name);
            if (remove_dir(buf))
                return -1;
        }
    }

    if (closedir(dp) < 0)
        return -1;

    if (remove(dir_path) < 0)
        return -1;

    return 0;
};

/*
 *
 */
int copy_file(const char* source, const char* dest, mode_t mode)
{

    stat_st fs;

    // doesn't exist?
    if (stat(source, &fs))
        return -1;

    fd_t in, out;

    IFNEG_RET(in  = z_open2(source, O_RDONLY), -1);
    IFNEG_RET(out = z_open3(dest, O_WRONLY | O_CREAT | O_TRUNC, mode), -1);

    char* buffer = (char*)malloc(500000);
    int res, count = fs.st_size;

    while (count) {
        IFLTE_RET(res = read(in, buffer, 500000), 0, -1);
        IFNEQ_RET(write(out, buffer, res), res, -1);
        count -= res;
    }

    IFNEG_RET(z_close(out), -1);
    IFNEG_RET(z_close(in) , -1);

    free(buffer);

    return 0;
};

/*
 *
 */
int prepare_sandbox(const char* path)
{
    stat_st fs;
    if (!stat(path, &fs)) {
        IFNEG_RET(remove_dir(path), -1);
    }

    // for unknown reason, we cant chmod the dir on creation
    // so we create it with weak (sort of unknown) permission,
    // and then we modify the permissions !
    IFNEG_RET(mkdir(path, S_IRWXU), -1);
    IFNEG_RET(unlock_sandbox(path), -1);
    return 0;
}

/*
 * unlock_sandbox sets 0744 on the sandbox dir
 */
int unlock_sandbox(const char* path)
{
    stat_st fs;
    IF_RET(stat(path, &fs), -1);
    IFNEG_RET(chmod(path, S_IRWXU | S_IWGRP | S_IXGRP | S_IWOTH | S_IXOTH), -1);
    return 0;
}

/*
 * lock_sandbox sets 0711 on the sandbox dir
 */
int lock_sandbox(const char* path)
{
    stat_st fs;
    IF_RET(stat(path, &fs), -1);
    IFNEG_RET(chmod(path, S_IRWXU | S_IXGRP | S_IXOTH), -1);
    return 0;
}

/*
 *
 */
int get_file_cont(const char* file_path,
                  char* buffer,
                  int max_len)
{
    stat_st fstat;
    IFNEG_RET(stat(file_path, &fstat), -1);

    fd_t fd_in;
    IFNEG_RET((fd_in = z_open2(file_path, O_RDONLY)), -1);
    IFNEQ_RET(read(fd_in, buffer,
                   MIN(fstat.st_size, max_len)),
              MIN(fstat.st_size, max_len),            -1);
    IFNEG_RET(z_close(fd_in),                         -1);

    return 0;
}

