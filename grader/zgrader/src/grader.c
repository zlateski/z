/*
 * Copyright (c) 2009-2012 z-training -- http://z-training.net
 *
 * AUTHORS:
 *   Aleksandar Zlateski (admin@z-trening.com, z@zlateski.com)
 *
 */

#include <stdio.h>

#include "zgrader/zgrader.h"

int main(int argc, char **argv)
{

    (void)argc;
    char* chrootDest = argv[2];
    char* logPath    = argv[3];
    char* graderPath = argv[4];
    int   graderUid  = atoi(argv[5]);
    int   graderGid  = atoi(argv[6]);

    int r = chroot(chrootDest);
    (void)r;
    init_grader(logPath);

    // Compile
    if  (strcmp(argv[1], "compile") == 0) {

        printf("compiling\n");

        char* sourceFile     = argv[7];
        int   language       = atoi(argv[8]);
        int   timeLimit      = atoi(argv[9]);

        double time;
        char   compileResult[1000001];

        int    result = perform_compile(graderPath,
                                        graderUid,
                                        graderGid,
                                        sourceFile,
                                        language,
                                        timeLimit,
                                        &time,
                                        compileResult);

        printf("%d %5.2f %s", result, time, compileResult);
    }

    // Grade
    if  (strcmp(argv[1], "grade") == 0) {

        char* taskName         = argv[7];
        int   timeLimit        = atoi(argv[8]);
        int   memLimit         = atoi(argv[9]);
        int   outLimit         = atoi(argv[10]);
        int   testCase         = atoi(argv[11]);
        int   language         = atoi(argv[12]);
        char* tasksPath        = argv[13];
        int   isInteractive    = atoi(argv[14]);

        double time;
        int    memory;
        int    score;

        int result = perform_grade(graderPath,
                                   graderUid,
                                   graderGid,
                                   graderUid,
                                   graderGid,
                                   tasksPath,
                                   taskName,
                                   testCase,
                                   isInteractive,
                                   language,
                                   timeLimit,
                                   memLimit,
                                   outLimit,
                                   &score,
                                   &time,
                                   &memory);

        printf("%d %d %5.2f %d", result, score, time, memory);

    }

    post_log();

    return 0;
}
