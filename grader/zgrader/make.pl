#!/usr/bin/perl -w

sub remove_binaries
{
    print `sudo rm -rf run compile grader`;
}

sub make_binaries
{
    my $arguments = "";
    if ( scalar(@_) > 0 )
    {
        $arguments = $_[0];
    }

    print "Compiling ./bin/run\n";
    print `gcc src/run.c src/zgrader/*.c -o bin/run -Wall -Wextra $arguments`;
    print "Compiling ./bin/compile\n";
    print `gcc src/compile.c src/zgrader/*.c -o bin/compile -Wall -Wextra $arguments`;
    print "Compiling ./bin/grader\n";
    print `gcc src/grader.c src/zgrader/*.c -o bin/grader -Wall -Wextra $arguments`;

    print "Changing the owner to root:root\n";
    print `sudo chown root:root ./bin/run ./bin/compile ./bin/grader`;
    print "Adding +xs to the file permissions\n";
    print `sudo chmod +xs ./bin/run ./bin/compile ./bin/grader`;
}

remove_binaries;

my $args = "";
my $has_debug = 0;

foreach ( @ARGV )
{
    if ( $_ eq "clean" )
    {
        print "Cleaning...\n";
        exit;
    }

    if ( $_ eq "opt" )
    {
        print "Compile optimized (-02)\n";
        $args .= " -O2";
    }

    if ( $_ eq "debug" )
    {
        print "Compile in debug mode\n";
        $has_debug = 1;
    }

    if ( $_ eq "static" )
    {
        print "Compile static binaries\n";
        $args .= " --static";
    }
}

if ( $has_debug == 0 )
{
    print "Compile without debug (-DNDEBUG)\n";
    $args .= " -DNDEBUG";
}

make_binaries($args);
