#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char* argv[])
{

    ifstream inFile(argv[1]);
    ifstream outFile(argv[2]);
    ifstream solFile(argv[3]);

    long long A, B;

    solFile >> A;

    if (!outFile.eof())
    {
        outFile >> B;
        cout << ((A == B) ? 1 : 0);
    }
    else
    {
        cout << 0;
    }

    return 0;
}
