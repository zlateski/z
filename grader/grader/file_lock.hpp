#ifndef ZTR_FILE_LOCK_HPP_INCLUDED
#define ZTR_FILE_LOCK_HPP_INCLUDED

#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>

#include "utility.hpp"

namespace ztr {

struct file_lock
{
    template< short int W >
    class generic_guard: noncopyable
    {
    private:
        int  fd_    ;
        bool locked_;

        void acquire_lock()
        {
            struct ::flock lock;
            lock.l_type   = W ;
            lock.l_whence = SEEK_SET;
            lock.l_start  = 0;
            lock.l_len    = 0;
            locked_ = (::fcntl(fd_, F_SETLKW, &lock) != -1);
            //std::cout << "acquire_lock locked? " << locked_ << std::endl;
        }

        void release_lock()
        {
            struct ::flock lock;
            lock.l_type   = F_UNLCK;
            lock.l_whence = SEEK_SET;
            lock.l_start  = 0;
            lock.l_len    = 0;
            locked_ = !(::fcntl(fd_, F_SETLK, &lock) != -1);
            //std::cout << "release_lock locked? " << locked_ << std::endl;
        }

    public:
        generic_guard(int fd)
            : fd_(fd)
            , locked_(false)
        {
            acquire_lock();
        }

        bool is_locked() const
        {
            return locked_;
        }

        ~generic_guard()
        {
            if ( locked_ )
            {
                release_lock();
            }
        }
    };

    typedef generic_guard<F_WRLCK> write_guard;
    typedef generic_guard<F_RDLCK> read_guard ;

};

} // namespace ztr

#endif // ZTR_FILE_LOCK_HPP_INCLUDED
