#ifndef ZTR_EXEC_RESULT_HPP_INCLUDED
#define ZTR_EXEC_RESULT_HPP_INCLUDED

#include <sys/resource.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <memory>

#include "types.hpp"
#include "limits.hpp"

namespace ztr {

struct exec_result
{
private:
    int  exit_status;
    long time       ;
    long memory     ;
    int  code       ;

    std::shared_ptr<rusage> rusage_;
    std::shared_ptr<limits> limits_;

public:
    exec_result()
        : exit_status(-1)
        , time()
        , memory()
        , code(result_code::na)
        , rusage_(new rusage)
        , limits_(new limits)
    { }

    exec_result(int status, const rusage& r, const limits& l)
        : exec_result()
    {
        load(status, r, l);
    }

    int get_status() const
    {
        return exit_status;
    }

    long get_memory() const
    {
        return memory;
    }

    long get_time() const
    {
        return time;
    }

    int get_code() const
    {
        return code;
    }

    int exit_signal() const
    {
        if (exit_status != -1)
        {
            return WTERMSIG(exit_status);
        }
        else
        {
            return -1;
        }    }

    int exit_code() const
    {
        if (exit_status != -1)
        {
            return WEXITSTATUS(exit_status);
        }
        else
        {
            return -1;
        }
    }

    void load(int status, const rusage& r, const limits& l)
    {
        *rusage_ = r;
        *limits_ = l;

        time = (r.ru_utime.tv_sec + r.ru_stime.tv_sec) * 1000
            + (r.ru_utime.tv_usec + r.ru_stime.tv_usec) / 1000;

        memory = r.ru_maxrss;

        exit_status = status;

        int exit_signal = WTERMSIG(status);
        int exit_code   = WEXITSTATUS(status);

        if (exit_code != 0)
        {
            code = result_code::no0;
            return;
        }

        switch (exit_signal)
        {
        case 0:
            break;
        case SIGXCPU:
        case SIGKILL:
            if ( memory > l.memory )
            {
                code = result_code::mle;
            }
            else
            {
                code = result_code::tle;
            }
            return;
        case SIGSEGV:
            if ( memory > l.memory )
            {
                code = result_code::mle;
            }
            else
            {
                code = result_code::seg;
            }
            return;
        case SIGIOT:
            code = result_code::mle;
            return;
        case SIGXFSZ:
            code = result_code::ole;
            return;
        default:
            code = result_code::rte;
            return;
        }

        if ( memory > l.memory )
        {
            code = result_code::mle;
            return;
        }

        if ( time > l.time )
        {
            code = result_code::tle;
            return;
        }

        code = result_code::ok;
    }

    template< class CharT, class Traits >
    friend ::std::basic_ostream< CharT, Traits >&
    operator<<( ::std::basic_ostream< CharT, Traits >& os,
                const exec_result& er )
    {
        return os << "[exec_result"
                  << " exit_status:" << er.exit_status
                  << " exit_signal:" << er.exit_signal()
                  << " exit_code: " << er.exit_code()
                  << " time: " << er.time
                  << " memory: " << er.memory
                  << " code: " << er.code << ']';
    }


};

inline exec_result
make_exec_result(int status, const rusage& r, const limits& l)
{
    return exec_result(status, r, l);
}



} // namespace ztr

#endif // ZTR_EXEC_RESULT_HPP_INCLUDED
