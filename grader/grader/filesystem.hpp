#ifndef ZTR_FILESYSTEM_HPP_INCLUDED
#define ZTR_FILESYSTEM_HPP_INCLUDED

#include <sys/types.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>

#include <string>
#include <cstring>
#include <cstdio>

namespace ztr {
namespace filesystem {

inline bool rmtree(const std::string& path)
{
    struct stat statbuf;

    if ( lstat(path.c_str(), &statbuf) < 0 )
    {
        // doesn't exist
        return true;
    }

    if ( S_ISREG(statbuf.st_mode) || S_ISLNK(statbuf.st_mode) )
    {
        // file or link - delete it
        return ::remove(path.c_str()) == 0;
    }

    if ( !S_ISDIR(statbuf.st_mode) )
    {
        // anything else -- error!
        return false;
    }

    DIR* dp = ::opendir(path.c_str());

    if ( dp == 0 )
    {
        return false;
    }

    dirent* dirp;

    while ( (dirp = ::readdir(dp)) )
    {
        if ( strcmp(dirp->d_name, ".") && strcmp(dirp->d_name, "..") )
        {
            if ( !rmtree(path + '/' + dirp->d_name) )
            {
                ::closedir(dp);
                return false;
            }
        }
    }

    if ( ::closedir(dp) < 0 )
    {
        return false;
    }

    return ::remove(path.c_str()) == 0;
}

inline bool rmtree(const char* path)
{
    return rmtree(std::string(path));
}

inline bool copy_file(const char* source, const char* dest, mode_t mode = 0644)
{
    int in = ::open(source, O_RDONLY);
    if ( in < 0 )
    {
        return false;
    }

    int out = ::open(dest, O_WRONLY | O_CREAT | O_TRUNC, mode);
    if ( out < 0 )
    {
        ::close(in);
        return false;
    }

    char buf[BUFSIZ];
    std::size_t size;

    while ((size = ::read(in, buf, BUFSIZ)) > 0)
    {
        ::write(out, buf, size);
    }

    ::close(in);
    ::close(out);

    return true;
}

inline bool copy_file(const std::string& source, const std::string& dest,
                      mode_t mode = 0644)
{
    return copy_file(source.c_str(), dest.c_str(), mode);
}

inline bool mkdir(const char* dir, mode_t mode = 0755)
{
    return ::mkdir(dir, mode) == 0;
}

inline bool mkdir(const std::string& dir, mode_t mode = 0755)
{
    return mkdir(dir.c_str(), mode);
}

inline bool chmod(const char* file, mode_t mode)
{
    return ::chmod(file, mode) == 0;
}

inline bool chmod(const std::string& file, mode_t mode)
{
    return chmod(file.c_str(), mode);
}

inline bool chown(const char* file, uid_t uid, gid_t gid)
{
    return ::chown(file, uid, gid) == 0;
}

inline bool chown(const std::string& file, uid_t uid, gid_t gid)
{
    return ::chown(file.c_str(), uid, gid) == 0;
}

inline bool file_exists(const char* fname)
{
    struct stat s;
    return stat(fname, &s) == 0;
}

inline bool file_exists(const std::string& fname)
{
    struct stat s;
    return stat(fname.c_str(), &s) == 0;
}

}} // namespace ztr::filesystem

#endif // ZTR_FILESYSTEM_HPP_INCLUDED
