#ifndef ZTR_PIPE_STREAM_HPP_INCLUDED
#define ZTR_PIPE_STREAM_HPP_INCLUDED

#include <unistd.h>

#include <type_traits>
#include <string>
#include <memory>
#include <cstddef>
#include <ostream>
#include <utility>
#include <cstdio>

#include "exception.hpp"

namespace ztr {

struct ipipestream
{
private:
    int fd;
    std::FILE* f;

public:
    ipipestream(): fd(-1) {};

    void set_fd(int fn)
    {
        fd = fn;
        f = ::fdopen(fd, "r");
    }

    void close()
    {
        if ( fd < 0 ) return;
        ::fclose(f);
        ::close(fd);
        fd = -1;
    }

    int get_fd() const
    {
        return fd;
    }

    std::FILE* get_file() const
    {
        return f;
    }

    template<typename T>
    typename std::enable_if<std::is_pod<T>::value, bool>::type
    read(T& t) const
    {
        if ( fd < 0 ) return false;
        return 1 == std::fread(static_cast<void*>(std::addressof(t)),
                               sizeof(T), 1, f );
    }

    bool read(std::string& t) const
    {
        if ( fd < 0 ) return false;
        std::size_t len;
        if (read(len))
        {
            t.resize(len);
            return len ==
                std::fread(const_cast<void*>
                           (static_cast<const void*>(t.c_str())),
                           1, len, f);
        }
        return false;
    }

};

struct opipestream
{
private:
    int fd;
    std::FILE* f;

    typedef void (*opipestream_manip_t)(opipestream*);


public:

    opipestream(): fd(-1) {};
    opipestream(int f): fd(f) {};

    void set_fd(int fn)
    {
        fd = fn;
        f = ::fdopen(fd, "w");
    }

    void close()
    {
        if ( fd < 0 ) return;
        ::fclose(f);
        ::close(fd);
        fd = -1;
        f = 0;
    }

    int get_fd() const
    {
        return fd;
    }

    std::FILE* get_file() const
    {
        return f;
    }

    template<typename T>
    typename std::enable_if<std::is_pod<T>::value, bool>::type
    write(const T& t)
    {
        if ( fd < 0 ) return false;
        return 1 == std::fwrite(static_cast<const void*>(std::addressof(t)),
                                sizeof(T), 1, f);
    }

    template<typename T>
    typename std::enable_if<std::is_same<T,std::string>::value, bool>::type
    write(const T& t)
    {
        if ( fd < 0 ) return false;
        if (!write(t.size())) return false;

        return t.size() ==
            std::fwrite(static_cast<const void*>(t.c_str()),
                        1, t.size(), f);
    }

    bool write(opipestream_manip_t m)
    {
        m(this);
        return true;
    }

    bool write(const char* s)
    {
        return write(std::string(s));
    }

    bool write(char* s)
    {
        return write(std::string(s));
    }

    void flush() const
    {
        if ( fd < 0 ) return;
        fflush(f);
    }

};


inline void flush(opipestream* out)
{
    out->flush();
}

struct pipe_pair
{
    ipipestream in ;
    opipestream out;
};

inline pipe_pair make_pipe_pair()
{
    pipe_pair ret;

    int pipefd[2];
    int r = ::pipe(pipefd);

    if ( r == 0 )
    {
        ret.in.set_fd(pipefd[0]);
        ret.out.set_fd(pipefd[1]);
    }

    return ret;
}

template<typename T>
ipipestream& operator>>(ipipestream& s, T& v)
{
    if ( !s.read(v) )
    {
        throw exception("ipipestream: broken pipe");
    }
    return s;
}

template<typename T>
opipestream& operator<<(opipestream& s, const T& v)
{
    if (!s.write(v))
    {
        throw exception("opipestream: broken pipe");
    }
    return s;
}

} // namespace ztr

#endif // ZTR_PIPE_STREAM_HPP_INCLUDED
