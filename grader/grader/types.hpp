#ifndef ZTR_TYPES_HPP_INCLUDED
#define ZTR_TYPES_HPP_INCLUDED

namespace ztr {

struct compilers
{
    static const int fpc     = 1;
    static const int gcc     = 2;
    static const int gpp     = 3;
    static const int gpp11   = 4;
    static const int clang   = 5;
    static const int clang11 = 6;
};

struct result_code
{
    static const int na  = -1;
    static const int ok  = 0;
    static const int tle = 1;
    static const int rte = 2;
    static const int mle = 3;
    static const int ole = 4;
    static const int seg = 5;
    static const int no0 = 6;
    static const int oth = 7;
};

struct exec_type
{
    static const int regular_t   = 1;
    static const int interactive = 2;
};


} // namespace ztr

#endif // ZTR_TYPES_HPP_INCLUDED
