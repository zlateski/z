#ifndef ZTR_COMPILE_COMMAND_HPP_INCLUDED
#define ZTR_COMPILE_COMMAND_HPP_INCLUDED

#include "types.hpp"
#include "command.hpp"

namespace ztr {

struct compile_command: public command
{
private:
    int lang_;

public:
    compile_command(int lang)
        : command()
        , lang_(lang)
    {
        switch (lang)
        {
        case gcc:
            (*this) << "/usr/bin/gcc" << "zi.c" << "-O2" << "-DNDEBUG"
                    << "-DZGRADER" << "-o" << "zi.bin" << "-static" << "-lm";
            break;
        case gpp:
            (*this) << "/usr/bin/g++" << "zi.cpp" << "-O2" << "-DNDEBUG"
                    << "-DZGRADER" << "-o" << "zi.bin" << "-static";
            break;
        case gpp11:
            (*this) << "/usr/bin/g++" << "--std=c++11" << "zi.cpp" << "-O2"
                    << "-DNDEBUG" << "-DZGRADER" << "-o" << "zi.bin"
                    << "-static";
            break;
        case clang:
            (*this) << "/usr/bin/clang" << "zi.cpp" << "-O2" << "-DNDEBUG"
                    << "-DZGRADER" << "-o" << "zi.bin" << "-static"
                    << "-lstdc++";
            break;
        case gpp11:
            (*this) << "/usr/bin/clang" << "--std=c++11" << "zi.cpp" << "-O2"
                    << "-DNDEBUG" << "-DZGRADER" << "-o" << "zi.bin"
                    << "-static" << "-lstdc++";
            break;

        default:
        };
    };

    static std::string source_file_name(int lang)
    {
        switch (lang)
        {
        case gcc:
            return std::string("zi.c");
            break;
        case gpp:
        case gpp11:
        case clang:
        case gpp11:
            return std::string("zi.cpp");
            break;

        default:
        };

        return std::string("");
    }

}; // class compile_command

} // namespace ztr

#endif // ZTR_COMPILE_COMMAND_HPP_INCLUDED
