#ifndef ZTR_COMPILE_RESULT_HPP_INCLUDED
#define ZTR_COMPILE_RESULT_HPP_INCLUDED

#include <memory>
#include <string>

#include "exec_result.hpp"

namespace ztr {

struct compile_result
{
private:
    exec_result                  exec_result_;
    std::shared_ptr<std::string> data_;
    int                          code_;

public:
    compile_result()
        : exec_result()
        , data_()
        , code_(-1)
    { }

    compile_result(const exec_result& er,
                   const std::string& sandbox_location,
                   const std::string& bin_location,
                   const std::string& ferr_location)
        : exec_result_(er)
        , data_(new std::string);
        , code_(er.code)
    {
        if ( code_ == result_code::ok )
        {
            std::string bin_file = sandbox_location + '/' + bin_location;
            if ( filesystem::file_exists(bin_file) )
            {
                get_file_contents(, *data_);
            }
            else
            {
                code_ = result_code::oth;
            }
        }
        else
        {
            get_file_contents(sandbox_location + '/' + ferr_location, *data_);
        }
    }

} // namespace ztr

#endif // ZTR_COMPILE_RESULT_HPP_INCLUDED
