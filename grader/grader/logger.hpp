#ifndef ZTR_LOGGER_HPP_INCLUDED
#define ZTR_LOGGER_HPP_INCLUDED

#include <unistd.h>

#include <iostream>
#include <utility>
#include <fstream>
#include <sstream>
#include <string>
#include <memory>
#include <chrono>
#include <iomanip>
#include <ctime>
#include <mutex>
#include <cstddef>

#include "utility.hpp"
#include "filesystem.hpp"
#include "file_lock.hpp"

namespace ztr {

class logger: noncopyable
{
private:
    struct create_token_tag {};

    template< typename F >
    class log_token
    {
    private:
        typedef F                      logger_type;
        typedef log_token<logger_type> this_type  ;

    private:
        std::ostringstream*  oss_;
        logger_type*         log_;

    public:
        log_token()
            : oss_(nullptr)
            , log_(nullptr)
        { }

        explicit log_token(logger_type* log)
            : oss_(new std::ostringstream)
            , log_(log)
        { }

        this_type& operator=(this_type&& other)
        {
            std::swap(oss_, other.oss_);
            std::swap(log_, other.log_);
            return *this;
        }

        log_token(this_type&& other)
            : oss_(nullptr)
            , log_(nullptr)
        {
            *this = std::forward<this_type>(other);
        }

        template< typename T >
        this_type& operator<<(const T& v)
        {
            if ( oss_ )
            {
                (*oss_) << v;
            }
            return *this;
        }

        ~log_token()
        {
            if ( oss_ && log_ )
            {
                log_->post(*oss_);
                delete oss_;
            }
        }
    };

private:
    typedef log_token<logger> token_type;

private:
    std::string                          name_  ;
    std::string                          indent_;
    int                                  fd_    ;

public:
    bool open(const std::string& fname)
    {
        if ( fd_ >= 0 )
        {
            ::close(fd_);
        }

        fd_ = ::open(fname.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0644);
        return (fd_ >= 0);
    }

    void write_string(const std::string& s) const
    {
        if ( fd_ >= 0 )
        {
            file_lock::write_guard g(fd_);

            const char* beg = s.c_str();
            std::size_t left = s.size();
            while ( left > 0 )
            {
                std::ptrdiff_t n = ::write(fd_, beg, left);
                left -= n;
                beg += n;
            }
        }
    }

public:
    logger()
        : name_()
        , indent_()
        , fd_(-1)
    { }

    explicit logger(const std::string& fname)
        : name_()
        , indent_()
        , fd_(-1)
    {
        open(fname);
    }

    explicit logger(const std::string& fname, const std::string& name)
        : name_(name)
        , indent_()
        , fd_(-1)
    {
        open(fname);
    }

    void set_name(const std::string& s)
    {
        name_ = s;
    }

    void inc_indent()
    {
        indent_ += "    ";
    }

    template< typename T >
    token_type operator<<(const T& t)
    {
        token_type ret(this);

        char buffer[32];

        std::time_t time = std::time(nullptr);
        std::strftime(buffer, 32, "%x %X", std::localtime(&time));

        ret << '[' << buffer << "] "
            << indent_ << '(' << ::getpid() << ") " << t;

        return ret;
    }

    void post(std::ostringstream& oss)
    {
        oss << "\n";
        write_string(oss.str());
    }

    void close()
    {
        if ( fd_ >= 0 )
        {
            ::close(fd_);
        }
        fd_ = -1;
    }

}; // class logger


} // namespace ztr

#endif // ZTR_LOGGER_HPP_INCLUDED
