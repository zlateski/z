#ifndef ZTR_UTILITY_HPP_INCLUDED
#define ZTR_UTILITY_HPP_INCLUDED

#include <string>
#include <sstream>
#include <fstream>

namespace ztr {

class noncopyable
{
protected:
    noncopyable() {}
    ~noncopyable() {}
private:
    noncopyable( const noncopyable& );
    const noncopyable& operator=( const noncopyable& );
};

template<typename T>
inline T& get_singleton()
{
    static T instance;
    return instance;
}

template <typename T>
std::string to_string(const T& v)
{
    std::ostringstream oss;
    oss << v;
    return oss.str();
}


inline bool get_file_contents(const char* fname, std::string& contents)
{
    std::ifstream in(fname, std::ios::in | std::ios::binary);
    if (in)
    {
        in.seekg(0, std::ios::end);
        contents.resize(in.tellg());
        in.seekg(0, std::ios::beg);
        in.read(&contents[0], contents.size());
        in.close();
        return true;
    }
    return false;
}

inline bool get_file_contents(const std::string& fname, std::string& contents)
{
    return get_file_contents(fname.c_str(), contents);
}

inline bool put_file_contents(const char* fname, std::string& contents)
{
    std::ofstream out(fname,
                      std::ios::out | std::ios::binary | std::ios::trunc);
    if (out)
    {
        out << contents;
        return true;
    }
    return false;
}

inline bool put_file_contents(const std::string& fname, std::string& contents)
{
    return put_file_contents(fname.c_str(), contents);
}

} // namespace ztr

#endif // ZTR_UTILITY_HPP_INCLUDED
