#include "file_lock.hpp"
#include "logger.hpp"
#include "exec_result.hpp"
#include "exec_worker.hpp"
#include <iostream>
#include "pipe_stream.hpp"
#include "types.hpp"
#include "command.hpp"
#include "filesystem.hpp"

#include <iostream>
#include <algorithm>
#include <functional>
#include "alarm_killer.hpp"
#include "files.hpp"
#include "executor.hpp"
#include <iostream>
#include <cstdio>
#include <iostream>
#include <type_traits>

#include <chrono>

struct pera_t
{
    std::string x;
};


ztr::exec_worker wrk;

// int get_mem_usage(pid_t pid)
// {
//     char file[1024];
//     char buffer[4001];

//     sprintf(file, "/proc/%d/status", pid);

//     int fd_in;
//     open(file, O_RDONLY);

//     int n;
//     n = read(fd_in, buffer, 4000);
//     close(fd_in);

//     buffer[n] = '\0';

//     char *hit;
//     hit = strstr(buffer, "VmPeak");

//     long vmpeak, vmsize, vmlck, vmhwm, vmrss, vmdata, vmstk, vmexe, vmlib, vmpte;
//     sscanf(hit, "VmPeak: %ld kB\nVmSize: %ld kB\n"
//            "VmLck: %ld kB\nVmHWM: %ld kB\nVmRSS: %ld kB\nVmData: %ld kB\n"
//            "VmStk: %ld kB\nVmExe: %ld kB\nVmLib: %ld kB\nVmPTE: %ld kB\n",
//            &vmpeak, &vmsize, &vmlck, &vmhwm,&vmrss, &vmdata, &vmstk, &vmexe,
//            &vmlib, &vmpte);

//     return (vmpeak - vmexe - vmpte);
// }


using namespace std;

int main(int argc, char* argv[])
{
    std::cout << std::is_integral<decltype(RLIMIT_AS)>::value << "\n";

    //std::cout << std::chrono::high_resolution_clock::now() << std::endl;

    ztr::logger flg("/tmp/aleks", "aleks");
    //logger << "pera";

    {
        flg << "aleks " << "mika";
        flg << "aleks " << "wmika";
        flg << "aleks " << "zmika";
    }

    //(logger << "12") << 13;
    //logger << "15" << 13 << ' ' << 14;
    {
        //ztr::logger_token<std::ostream> lt(&std::cout);
        //lt << "pera" << " zika";
    }

    //exit(0);

    static_cast<void>(argv);
    ztr::command  cmd("/usr/bin/clang");
    cmd << "test.cpp" << "-o" << "test" << "-Wall" << "-DNDEBUG" << "-DZGRADER"
        << "-O2" << "-static" << "-lstdc++";


     //ztr::command cmd("./test");
    std::string   sandbox = "/home/zlateski/code/z/grader/grader/sbx";
    std::string   fin("/dev/null")     ;
    std::string   fout("err.txt")   ;
    std::string   ferr("err.txt")   ;

    struct ztr::limits lmts = ztr::make_limits(1000, 100000, 0, 0) ;

    uid_t          uid = 20000  ;
    gid_t          gid = 20000  ;

    bool           chrooted = false;


    ztr::exec_result xxx =
        wrk.execute(sandbox, cmd, fin, fout, ferr, lmts, uid, gid, chrooted);

    std::cout << "here" << std::endl;

    xxx =
        wrk.execute(sandbox, cmd, fin, fout, ferr, lmts, uid, gid, chrooted);

    xxx =
        wrk.execute(sandbox, cmd, fin, fout, ferr, lmts, uid, gid, chrooted);

    std::cout << xxx << " !!!!!!!" << std::endl;

    while(1);


    int xd = S_IRWXU | S_IRGRP | S_IROTH;
    std::cout << std::oct << xd << std::endl;


    std::cout << std::is_pod<rusage>::value << std::endl;
    exit(0);

    ztr::pipe_pair pp = ztr::make_pipe_pair();

    struct ztr::limits lim;
    lim = ztr::make_limits(30, 30);

    pp.out << ztr::make_limits(10, 20) << ztr::flush;
    pp.in >> lim;

    std::cout << lim.time << ' ' << lim.memory << ' '
              << lim.process << ' ' << lim.output << std::endl;



    // worker w;

    // for ( int i = 10; i < 20; ++i )
    // {
    //     w.test(i);
    // }

    //sleep(1000);

    std::cout << std::is_pod<pera_t>::value << std::endl;
    exit(0);

    struct rusage rusage;
    getrusage(RUSAGE_SELF, &rusage);
    std::cout << argc << " RUSAGE from 1 : " << rusage.ru_maxrss << std::endl;

    char* pera2;
    int nx = 10000;
    while (--nx) {
        pera2 = new char[1000];
        pera2[0] = 3;
        for ( int i = 1; i < 1000; ++i )
            pera2[i] +=  pera2[i-1];
    }

    int ff = fork();

    if ( ff == 0 )
    {
        struct rusage r;
        std::cout << getrusage(RUSAGE_SELF, &r) << " zzz " << r.ru_maxrss << std::endl;
        std::cout << "PID: getpid(): " << getpid() << std::endl;
        //std::cout << get_mem_usage(getpid()) << " zzz " << std::endl;
        execl("/usr/bin/find", "/usr/bin/find", "/", 0);
        sleep(30);
    }

    std::cout << ff << std::endl;

    //std::cout << argc << " Child2 from 1 : " << get_mem_usage(ff) << std::endl;
    int status;
    wait4(ff, &status, 0, &rusage);
    std::cout << " aaa " << rusage.ru_maxrss << std::endl;
    std::cout << getrusage(RUSAGE_CHILDREN, &rusage) << " bbb " << rusage.ru_maxrss << std::endl;
    std::cout << argc << " Child2 from 1 : " << rusage.ru_maxrss << std::endl;
        //


    sleep(1000);

    exit(0);

//         //execl("./b.out", "./b.out", NULL);
//         ztr::executor exe("/usr/bin/clang");
//         //exe << 3 << 2;
//         exe << "m2.cpp" << "-o" << "b.out" << "-lstdc++" << "-static";
//         exe.exec();

//         //std::cout << (b.apply() ? "yes\n" : "no\n");

//         //char* pera;

// //         while (1) {
// //             pera[3] = 0;
// //             pera[4] = 1/pera[3];
// //             pera = new char[1000];
// //             pera[123123] = 23;
// //             pera[23213] = pera[123123] + 3;
// // //for ( int i = 1; i < 1000; ++i )
// //             //    pera[i] +=  pera[i-1];
// //         };
//     }
//     else
//     {
//         ztr::alarm_killer.signup(ff, l.time*2);


//         struct rusage r;
//         int status;

//         while (true)
//         {
//             wait4(ff, &status, 0, &r);
//             if ( WIFEXITED(status) || WIFSIGNALED(status) )
//             {
//                 break;
//             }
//             else
//             {
//                 // ERROR?
//             }
//         }

//         ztr::alarm_killer.clear();



    std::cout << static_cast<unsigned int>(ztr::compilers::gpp) << '\n';
    //ztr::compilers ce(3);

    std::string pera;
    std::cout << ztr::get_file_contents("./main.cpp", pera) << std::endl;
    std::cout << pera << std::endl;

    std::cout << ztr::put_file_contents("./main2.cpp", pera) << std::endl;


    int x = S_IRWXU | S_IXGRP | S_IXOTH;
    std::cout << std::oct << x << std::endl;

    std::cout << sizeof(gid_t) << std::endl;

    ztr::command c("/bin/ls");
    c << "-alh";

    ztr::command c2 = c;
    c2.exec();

    struct rlimit rlimit;

    std::cout << sizeof(rlimit.rlim_cur) << std::endl;
    std::cout << sizeof(long) << std::endl;

    std::cout << ztr::filesystem::mkdir("./zika", 0744) << std::endl;
    std::cout << ztr::filesystem::copy_file("./main.cpp", "./pera/pera.hpp");
    //std::cout << (ztr::filesystem::rmtree("./pera") ? "ok\n" : "not\n");

    return 0;


//     //std::cout << (ztr::alarm_killer.signup(1,2,1110) ? "Y\n" : "N\n");

//     //std::cout << (ztr::alarm_killer.signup(1,2,0) ? "Y\n" : "N\n");
//     //while(1) {}

//     // int x = ztr::files.open("./pera.txt",
//     //                         O_WRONLY | O_CREAT | O_TRUNC,
//     //                         S_IRWXU  | S_IRGRP | S_IROTH);

//     // int y = fork();

//     // if ( y == 0 )
//     // {
//     //     ztr::files.dup2(x, 1);
//     //     ztr::files.close(x);
//     //     //FILE* f = fdopen(0, "w");
//     //     printf("aleks\n");
//     //     std::cout << STDIN_FILENO << ' ' << STDOUT_FILENO << ' ' << STDERR_FILENO << '\n';
//     //     //ztr::files.close_other();
//     //     std::cout << argc << "Child\n" << std::endl;

//     //     while(1);
//     // }
//     // else
//     // {
//     //     std::cout << "Parent (" << getpid() << ") of " << y << std::endl;
//     //     while(1);
// //}

//     int ff = fork();

//     ztr::limits l(10000, 300000, 0, 100000);
//     ztr::limits b = l;

//     if ( ff == 0 )
//     {
//         b.apply();

//         //execl("./b.out", "./b.out", NULL);
//         ztr::executor exe("/usr/bin/clang");
//         //exe << 3 << 2;
//         exe << "m2.cpp" << "-o" << "b.out" << "-lstdc++" << "-static";
//         exe.exec();

//         //std::cout << (b.apply() ? "yes\n" : "no\n");

//         //char* pera;

// //         while (1) {
// //             pera[3] = 0;
// //             pera[4] = 1/pera[3];
// //             pera = new char[1000];
// //             pera[123123] = 23;
// //             pera[23213] = pera[123123] + 3;
// // //for ( int i = 1; i < 1000; ++i )
// //             //    pera[i] +=  pera[i-1];
// //         };
//     }
//     else
//     {
//         ztr::alarm_killer.signup(ff, l.time*2);


//         struct rusage r;
//         int status;

//         while (true)
//         {
//             wait4(ff, &status, 0, &r);
//             if ( WIFEXITED(status) || WIFSIGNALED(status) )
//             {
//                 break;
//             }
//             else
//             {
//                 // ERROR?
//             }
//         }

//         ztr::alarm_killer.clear();

//         //std::cout << "Status

//         //wait4(pid,

//         //getrusage(RUSAGE_SELF, &r);

//         ztr::executor::result re(status, l, r);

//         std::cout << WTERMSIG(status) << ' ' << SIGXCPU << std::endl;

//         std::cout << re.status << std::endl;
//         std::cout << re.memory << std::endl;
//         std::cout << re.time << std::endl;
//         std::cout << re.code << std::endl;

//     }
}

