#ifndef ZTR_FILES_HPP_INCLUDED
#define ZTR_FILES_HPP_INCLUDED

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <set>

#include "singleton.hpp"

namespace ztr {

class files_impl
{
private:
    std::set<int> descriptors_;

public:
    int open(const char* pathname, int flags)
    {
        int fd = ::open(pathname, flags);
        if ( fd != -1 )
        {
            descriptors_.insert(fd);
        }
        return fd;
    }

    int open(const char* pathname, int flags, mode_t mode)
    {
        int fd = ::open(pathname, flags, mode);
        if ( fd != -1 )
        {
            descriptors_.insert(fd);
        }
        return fd;
    }

    int close(int fd)
    {
        if ( descriptors_.count(fd) )
        {
            descriptors_.erase(fd);
        }
        return ::close(fd);
    }

    int pipe(int pipefd[2])
    {
        if ( ::pipe(pipefd) == 0 )
        {
            descriptors_.insert(pipefd[0]);
            descriptors_.insert(pipefd[1]);
            return 0;
        }
        return -1;
    }

    int dup2(int with_this, int to_replace)
    {
        if ( descriptors_.count(with_this) )
        {
            descriptors_.insert(to_replace);
            return ::dup2(with_this, to_replace);
        }
        else
        {
            return -1;
        }
    }

    int close_other()
    {
        std::set<int> left_;
        for ( std::set<int>::const_iterator it = descriptors_.begin();
              it != descriptors_.end(); ++it )
        {
            if ( (*it) > 2 )
            {
                if ( ::close(*it) == -1 )
                {
                    return -1;
                }
            }
            else
            {
                left_.insert(*it);
            }
        }
        left_.swap(descriptors_);
        return 0;
    }

};

namespace {
files_impl& files = get_singleton<files_impl>();
}

}

#endif // ZTR_FILES_HPP_INCLUDED
