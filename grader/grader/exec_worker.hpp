#ifndef ZTR_EXEC_WORKER_HPP_INCLUDED
#define ZTR_EXEC_WORKER_HPP_INCLUDED

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <utility>
#include <string>
#include <fstream>

#include "exec_result.hpp"
#include "alarm_killer.hpp"
#include "limits.hpp"
#include "types.hpp"
#include "pipe_stream.hpp"
#include "command.hpp"
#include "exception.hpp"
#include "filesystem.hpp"
#include "utility.hpp"
#include "logger.hpp"

namespace ztr {

struct exec_worker
{
private:
    ipipestream    in_ ;
    opipestream    out_;
    logger         log_;

    void do_regular()
    {
        command       cmd;

        std::string   sandbox;
        std::string   fin    ;
        std::string   fout   ;
        std::string   ferr   ;

        struct limits limits ;

        uid_t          uid   ;
        gid_t          gid   ;

        bool           chrooted;

        in_ >> sandbox >> cmd >> fin >> fout >> ferr
            >> limits >> uid >> gid >> chrooted;

        if ( fin == "" )
        {
            fin = "/dev/null";
        }

        pid_t pid = fork();
        if ( pid < 0 )
        {
            throw exception("exec_worker: can't fork");
        }

        if ( pid == 0 )
        {
            log_.inc_indent();

            in_.close();
            out_.close();

            ::close(STDIN_FILENO);
            ::close(STDOUT_FILENO);
            ::close(STDERR_FILENO);

            //
            // Move to the sandbox
            //

            log_ << "Chdir to: " << sandbox;

            if ( ::chdir(sandbox.c_str()) )
            {
                exit(13);
            }

            //
            // Setup input/output/error files
            //

            log_ << "Setting up io: " << fin
                 << ' ' << fout << ' ' << ferr;

            int fno = -1;

            fno = ::open(fin.c_str(), O_RDONLY);
            if ( fno < 0 ) exit(13);
            ::dup2(fno, STDIN_FILENO);

            fno = ::open(fout.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0600);
            if ( fno < 0 ) exit(13);
            ::dup2(fno, STDOUT_FILENO);

            if ( ferr != fout )
            {
                fno = ::open(ferr.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0600);
                if ( fno < 0 ) exit(13);
                ::dup2(fno, STDERR_FILENO);
            }
            else
            {
                ::dup2(fno, STDERR_FILENO);
            }

            //
            // Apply limits
            //

            log_ << "Setting up limits: " << limits;

            if ( !limits.apply() )
            {
                exit(13);
            }

            //
            // Chroot execution
            //

            if ( chrooted )
            {
                log_ << "Setting chroot: " << sandbox;
                if ( ::chroot(sandbox.c_str()) )
                {
                    exit(13);
                }
            }

            //
            // Set gid/uid
            //

            if ( gid )
            {
                log_ << "Setting gid: " << gid;
                if ( ::setgid(gid) )
                {
                    exit(13);
                }
            }

            if ( uid )
            {
                log_ << "Setting uid: " << uid;
                if ( ::setuid(uid) )
                {
                    exit(13);
                }
            }

            log_ << "Executing the command " << cmd;
            log_.close();

            cmd.exec();
            exit(13);
        }

        log_ << "Settig up alarm in: " << limits.time;

        alarm_killer.signup(pid, limits.time);

        int status = 9;
        struct rusage usage;

        log_ << "Going in the while loop";

        while ( true )
        {
            log_ << "wait4(" << pid << ")";

            wait4(pid, &status, 0, &usage);

            log_ << "status: " << status << " :: "
                 << (WIFEXITED(status) ? "exited " : "not exited ")
                 << (WIFSIGNALED(status) ? "signaled" : "not signaled")
                ;

            if ( WIFEXITED(status) || WIFSIGNALED(status) )
            {
                break;
            }
            else
            {
                // ???
            }
        }

        log_ << "Killing the alarm";

        alarm_killer.clear();

        log_ << "Piping results";
        out_ << status << usage << flush;
        log_ << "Done";
    }

public:
    exec_worker()
    {
        pipe_pair to_worker   = make_pipe_pair();
        pipe_pair from_worker = make_pipe_pair();

        int pid = fork();

        if ( pid < 0 )
        {
            throw exception("exec_worker: can't fork");
        }

        if ( pid == 0 )
        {
            log_.inc_indent();

            in_ = to_worker.in;
            to_worker.out.close();

            out_ = from_worker.out;
            from_worker.in.close();

            log_.open("/tmp/log_pid_" + to_string(getpid()));

            rusage usage;
            ::getrusage(RUSAGE_SELF, &usage);

            log_ << "worker(" << getpid() << ") starting: max_rss: "
                 << usage.ru_maxrss;

            while (true)
            {
                int type;
                in_ >> type;
                if ( type == 1 )
                {
                    do_regular();
                }
                else
                {
                    // ?
                }
            }
        }
        else
        {
            log_.open("/tmp/log_pid_" + to_string(pid)
                      + "_parent_" + to_string(getpid()));

            in_ = from_worker.in;
            from_worker.out.close();
            out_ = to_worker.out;
            to_worker.in.close();
        }
    }

    ~exec_worker()
    {
        in_.close();
        out_.close();
    }

    exec_result execute(const std::string&   sandbox,
                        const command&       cmd,
                        const std::string&   fin,
                        const std::string&   fout,
                        const std::string&   ferr,
                        const limits&        lmts,
                        uid_t                uid,
                        gid_t                gid,
                        bool                 chrooted)
    {
        out_ << static_cast<int>(1);
        out_ << sandbox << cmd << fin << fout << ferr
             << lmts << uid << gid << chrooted << flush;

        log_ << "execute(...): sent data";

        std::pair<int, rusage> ret;

        log_ << "execute(...): reading results data";

        in_ >> ret.first >> ret.second;

        int exit_signal = WTERMSIG(ret.first);
        int exit_code   = WEXITSTATUS(ret.first);

        log_ << "execute(...): got results: " << ret.first
             << " exit signal: " << exit_signal
             << " exit code: " << exit_code;

        log_ << "all done";

        return exec_result(ret.first, ret.second, lmts);
    }


};


} // namespace ztr

#endif // ZTR_EXEC_WORKER_HPP_INCLUDED
