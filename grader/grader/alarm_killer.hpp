#ifndef ZTR_ALARM_KILLER_HPP_INCLUDED
#define ZTR_ALARM_KILLER_HPP_INCLUDED

#include <signal.h>
#include <sys/time.h>

#include "utility.hpp"

namespace ztr {

inline void alarm_handler(int);

class alarm_killer_impl
{
private:
    pid_t pid1, pid2;

    bool set_timer(int time)
    {
        struct itimerval t;
        t.it_interval.tv_sec = t.it_interval.tv_usec = 0;
        t.it_value.tv_sec = time / 1000;
        t.it_value.tv_usec = 1000 * ( time % 1000 );
        return setitimer(ITIMER_REAL, &t, 0) == 0;
    }

public:
    void kill_them()
    {
        if (pid1) kill(pid1, SIGKILL);
        if (pid2) kill(pid2, SIGKILL);
    }

    bool signup(pid_t p, int time)
    {
        pid1 = p; pid2 = 0;
        if ( ::signal(SIGALRM, alarm_handler) == SIG_ERR ) return false;
        return set_timer(time);
    }

    bool signup(pid_t p1, pid_t p2, int time)
    {
        pid1 = p1;
        pid2 = p2;
        if ( ::signal(SIGALRM, alarm_handler) == SIG_ERR ) return false;
        return set_timer(time);
    }

    bool clear()
    {
        if ( ::signal(SIGALRM, SIG_IGN) == SIG_ERR ) return false;
        pid1 = pid2 = 0;
        return set_timer(0);
    }
};

namespace {
alarm_killer_impl& alarm_killer = get_singleton<alarm_killer_impl>();
}

inline void alarm_handler(int)
{
    alarm_killer.kill_them();
}

} // namespace ztr

#endif // ZTR_ALARM_KILLER_HPP_INCLUDED
