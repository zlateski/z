#ifndef ZTR_SINGLETON_HPP_INCLUDED
#define ZTR_SINGLETON_HPP_INCLUDED

namespace ztr {

template<class T>
class singleton: private T
{
private:

    singleton() {};
    ~singleton() {};
    singleton( const singleton<T>& );
    singleton& operator=( const singleton<T>& );

public:

    typedef singleton<T> type;

    static T& instance()
    {
        static singleton<T> instance;
        return instance;
    }

};

template<typename T>
inline T& get_singleton2()
{
    static T instance;
    return instance;
}

} // namespace ztr

#endif // ZTR_SINGLETON_HPP_INCLUDED
