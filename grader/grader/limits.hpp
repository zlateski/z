#ifndef ZTR_EXEC_LOOP_HPP_INCLUDED
#define ZTR_EXEC_LOOP_HPP_INCLUDED

#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <iostream>

namespace ztr {

struct limits
{
    long time   ;
    long memory ;
    long process;
    long output ;

    bool apply() const
    {
        struct ::rlimit rlim;

        if ( memory > 0 )
        {
            rlim.rlim_cur = rlim.rlim_max = (memory << 10) + (4 << 20);
            if ( setrlimit(RLIMIT_AS, &rlim) < 0 ) return false;
        }

        if ( process > 0 )
        {
            rlim.rlim_cur = rlim.rlim_max = process;
            if ( setrlimit(RLIMIT_NPROC, &rlim) < 0 ) return false;
        }

        if ( time > 0 )
        {
            rlim.rlim_cur = rlim.rlim_max = ((time) / 1000) + 1;
            if ( setrlimit(RLIMIT_CPU, &rlim) < 0 ) return false;
        }

        if ( output > 0 )
        {
            rlim.rlim_cur = rlim.rlim_max = (output << 10);
            if ( setrlimit(RLIMIT_FSIZE, &rlim) < 0 ) return false;
        }

        return true;
    }
};

inline limits make_limits(long t = 0, long m = 0, long p = 0, long o = 0)
{
    return limits{t,m,p,o};
}

template< class CharT, class Traits >
::std::basic_ostream< CharT, Traits >&
operator<<( ::std::basic_ostream< CharT, Traits >& os,
            const limits& l )
{
    return os << "[limits time: " << l.time
              << " memory: " << l.memory
              << " process: " << l.process
              << " output: " << l.output;
}


} // namespace ztr

#endif // ZTR_EXEC_LOOP_HPP_INCLUDED
