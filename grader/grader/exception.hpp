#ifndef ZTR_EXCEPTION_HPP_INCLUDED
#define ZTR_EXCEPTION_HPP_INCLUDED

#include <exception>
#include <string>

namespace ztr {

class exception: public std::exception
{
protected:
    const std::string message_;

public:
    exception(): message_()
    {
    }

    exception( const std::string& message ):
        message_( message )
    {
    }

    virtual ~exception() noexcept
    {
    }

    virtual const char* what() const noexcept
    {
        if ( message_.empty() )
        {
            return "default exception";
        }
        else
        {
            return message_.c_str();
        }
    }

};

} // namespace ztr

#endif // ZTR_EXCEPTION_HPP_INCLUDED
