#ifndef ZTR_EXECUTOR_HPP_INCLUDED
#define ZTR_EXECUTOR_HPP_INCLUDED

#include <sys/time.h>
#include <sys/resource.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <string>

#include <boost/lexical_cast.hpp>

#include "files.hpp"
#include "limits.hpp"

namespace ztr {


class executor
{
private:
    typedef struct rusage rusage_t;

public:
    struct result
    {
        static const int ok  = 0;
        static const int tle = 1;
        static const int rte = 2;
        static const int mle = 3;
        static const int ole = 4;
        static const int seg = 5;
        static const int no0 = 6;
        static const int oth = 7;

        int  status;
        long time  ;
        long memory;
        int  code  ;

        result(int s, const limits& l, const rusage_t& r )
            : status(s), time(0), memory(0), code(oth)
        {

            int t = (r.ru_utime.tv_sec + r.ru_stime.tv_sec) * 1000
                + (r.ru_utime.tv_usec + r.ru_stime.tv_usec) / 1000;

            int m = r.ru_maxrss;

            memory = m;
            time   = t;

            int exit_signal = WTERMSIG(s);
            int exit_code   = WEXITSTATUS(s);

            if (exit_code != 0)
            {
                code = result::no0;
                return;
            }

            switch (exit_signal)
            {
            case 0:
                break;
            case SIGXCPU:
            case SIGKILL:
                code = ( memory > l.memory ) ? result::mle : result::tle;
                return;
            case SIGSEGV:
                code = ( memory > l.memory ) ? result::mle : result::seg;
                return;
            case SIGIOT:
                code = result::mle;
                return;
            case SIGXFSZ:
                code = result::ole;
                return;
            default:
                code = result::rte;
                return;
            }

            if ( m > l.memory )
            {
                code = result::mle;
                return;
            }

            if ( t > l.time )
            {
                code = result::tle;
                return;
            }

            code = result::ok;

            memory = m;
            time   = t;
        }


    };


private:
    std::string exec_;
    char*       argv_[21];
    std::string sarg_[21];
    int         argc_;

public:
    explicit executor(const std::string& bin)
        : exec_(bin), argv_(), sarg_(), argc_(0)
    {
        argv_[0] = const_cast<char*>(exec_.c_str());
        argc_ = 1;
    }

    template<typename T>
    friend executor& operator<<(executor& e, const T& v)
    {
        if ( e.argc_ < 20 )
        {
            e.sarg_[e.argc_] = boost::lexical_cast<std::string>(v);
            e.argv_[e.argc_] = const_cast<char*>(e.sarg_[e.argc_].c_str());
            ++e.argc_;
            e.argv_[e.argc_] = 0;
        }
        return e;
    }

    void exec() const
    {
        execv(argv_[0], argv_);
        exit(1);
    }

};

} // namespace ztr

#endif // ZTR_EXECUTOR_HPP_INCLUDED
