#ifndef ZTR_COMMAND_HPP_INCLUDED
#define ZTR_COMMAND_HPP_INCLUDED

#include <sys/types.h>
#include <unistd.h>

#include <vector>
#include <string>
#include <iostream>

#include "utility.hpp"
#include "pipe_stream.hpp"

namespace ztr {

struct command
{
private:
    std::vector<std::string> args_;

public:
    command(): args_() {};

    command(const std::string& s)
        : args_()
    {
        args_.push_back(s);
    }

    command(const char* s)
    {
        args_.push_back(std::string(s));
    }

    template<typename T>
    friend command& operator<<(command& e, const T& v)
    {
        e.args_.push_back(to_string(v));
        return e;
    }

    template<typename T>
    command& add_argument(const T& v)
    {
        args_.push_back(to_string(v));
        return *this;
    }

    void exec() const
    {
        typedef char* char_ptr;
        char_ptr* args = new char_ptr[args_.size()+1];

        for ( std::size_t i = 0; i < args_.size(); ++i )
        {
            args[i] = const_cast<char*>(args_[i].c_str());
        }

        args[args_.size()] = 0;
        execv(args[0], args);

        ::exit(13);
    }

    friend opipestream& operator<<(opipestream& o, const command& c)
    {
        o << c.args_.size();
        for ( std::size_t i = 0; i < c.args_.size(); ++i )
        {
            o << c.args_[i];
        }
        return o;
    }

    friend ipipestream& operator>>(ipipestream& ips, command& c)
    {
        std::size_t l;
        ips >> l;
        c.args_.resize(l);
        for ( std::size_t i = 0; i < l; ++i )
        {
            ips >> c.args_[i];
        }
        return ips;
    }

    template< class CharT, class Traits >
    friend ::std::basic_ostream< CharT, Traits >&
    operator<<( ::std::basic_ostream< CharT, Traits >& os,
                const command& c )
    {
        os << "[command:";
        for ( std::size_t i = 0; i < c.args_.size(); ++i )
        {
            os << ' ' << c.args_[i];
        }
        return os << "]";
    }

}; // class command

} // namespace ztr

#endif // ZTR_COMMAND_HPP_INCLUDED
