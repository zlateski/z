#ifndef ZTR_SANDBOX_HPP_INCLUDED
#define ZTR_SANDBOX_HPP_INCLUDED

#include <string>

#include "utility.hpp"
#include "filesystem.hpp"
#include "compile_command.hpp"

namespace ztr
{

class sandbox: noncopyable
{
private:
    const std::string path_;
    unsigned int      id_  ;
    const std::string full_path_;

public:
    sandbox(const std::string& path, unsigned int id)
        : path_(path)
        , id_(id)
        , full_path_(path + '/' + to_string(id))
    { }

    bool clear() const
    {
        return filesystem::rmtree(full_path_);
    }

    bool reset() const
    {
        if ( !clear() ) return false;
        return mkdir(full_path_, 0700);
    }

    bool compile(const std::string& source, int lang)
    {
        std::string source_name = compile_command::;
        file_put_contents(full_path_ + '/' +
        compile_command cmd(lang);

    }

}; // class sandbox

} // namespace ztr

#endif // ZTR_SANDBOX_HPP_INCLUDED
